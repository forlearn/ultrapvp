package pl.mc4e.ery65.configuration;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.minecraft4ever.glowna.UltraPvp;

public class LogsConfig {
    
    private static File file;
    
    private static FileConfiguration cfg;
    
    public static boolean ALL_SCB_INFO;
    public static boolean MIN_SCB_INFO;
    public static boolean ALL_REW_INFO;
    public static boolean MIN_REW_INFO;
    public static boolean ALL_DB_INFO;
    public static boolean MIN_DB_INFO;
    public static boolean ALL_RANG_INFO;
    public static boolean MIN_RANG_INFO;
    public static boolean ALL_OTHER_INFO;
    public static boolean MIN_OTHER_INFO;
    
    static {
        load();
    }
    
    private static void load(){
        
        file = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "logs-preferences.yml");
        
        if (!file.exists()){
            UltraPvp.getInstance().getDataFolder().mkdirs();
            UltraPvp.getPrivateLogger().println("Creating default file logs-preferences.yml");
            UltraPvp.copy(UltraPvp.getInstance().getResource("Resources/logs-preferences.yml"), file);
        }
        
        cfg = YamlConfiguration.loadConfiguration(file);
        
        ALL_SCB_INFO = cfg.getBoolean("scoreboard-log.show-all-info", false);
        MIN_SCB_INFO = cfg.getBoolean("scroreboard-log.show-min-info", true);
        ALL_REW_INFO = cfg.getBoolean("rewards-log.show-all-info", false);
        MIN_REW_INFO = cfg.getBoolean("rewards-log.show-min-info", true);
        ALL_DB_INFO = cfg.getBoolean("database-log.show-min-info", false);
        MIN_DB_INFO = cfg.getBoolean("database-log.show-min-info", true);
        ALL_RANG_INFO = cfg.getBoolean("rangs-log.show-min-info", false);
        MIN_RANG_INFO = cfg.getBoolean("rangs-log.show-min-info", true);
        ALL_OTHER_INFO = cfg.getBoolean("others-log.show-min-info", false);
        MIN_OTHER_INFO = cfg.getBoolean("others-log.show-min-info", true);
        
    }
    
    public static void reload(){
        load();
    }
    
}
