package pl.mc4e.ery65.configuration;

import java.io.File;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.minecraft4ever.glowna.UltraPvp;

public class PluginConfig {
    
    public static String LANG;
    public static String HOST;
    public static String DATABASE;
    public static String USER;
    public static String PASSWORD;
    public static String TO_REPLACE_LVL;
    public static String TO_REPLACE_RANG;
    public static String LVL_NAME;
    public static String RANG_COLOR;
    public static String BASE_NAME;
    public static String NICK_COLOR;
    
    public static boolean USE_RANGS;
    public static boolean USE_LVL_ON_CHAT;
    public static boolean USE_RANG_ON_CHAT;
    public static boolean USE_MYSQL;
    public static boolean NGR_IF_MAX_LVL;
    public static boolean USE_NEGATIVE_BALANCE;
    public static boolean USE_LVL_BELLOW_NAME;
    public static boolean USE_RANG_BELLOW_NAME;
    public static boolean ENABLE_SCOREBOARDS;
    public static boolean ENABLE_SIDEBAR_SCOREBOARDS;
    public static boolean DEBUG;
    public static boolean LOGGING;
    public static boolean DATE_NAME;
    public static boolean REMOVE_OLD;
    
    public static int REMOVE_AFTER;
    public static int PORT;
    public static int DEF_KILLS;
    public static int KILLS_SESSION;
    public static int MAX_LVL;
    public static int ALLOW_CHAT_LVL;
    
    public static long AUTOSAVE_TIME;
    
    public static double HOW_MANY;
    
    public static List<String> LOCKED_WORLDS;
    
    private static FileConfiguration cfg;
    
    static{
        load();
    }
    
    private static void load(){
        File ustawienia = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "config.yml");
        cfg = YamlConfiguration.loadConfiguration(ustawienia);
        
        LANG = cfg.getString("jezyk", "pl");
        LVL_NAME = cfg.getString("lvl-name", "&aPoziom").replaceAll("&", "§");
        RANG_COLOR = cfg.getString("rang-color", "&b").replaceAll("&", "§");
        
        USE_NEGATIVE_BALANCE = cfg.getBoolean("Kasa.ujemnybalans", false);
        LOGGING = cfg.getBoolean("logging-to-file", false);
        DATE_NAME = cfg.getBoolean("date-name", true);
        REMOVE_OLD = cfg.getBoolean("remove-old", true);
        DEBUG = cfg.getBoolean("debug", false);
        USE_RANGS = cfg.getBoolean("use-rangs", true);
        LOCKED_WORLDS = cfg.getStringList("LockedWorlds");
        
        AUTOSAVE_TIME = cfg.getLong("autosave", 300);
        
        ConfigurationSection lvl = cfg.getConfigurationSection("Poziomy");
        
        NGR_IF_MAX_LVL = lvl.getBoolean("nagrody-przy-max-lvl", true);
        
        DEF_KILLS = lvl.getInt("domyslne-zabicia", 1);
        KILLS_SESSION = lvl.getInt("sesja-zabic", 10);
        MAX_LVL = lvl.getInt("max-poziom", 10000);
        REMOVE_AFTER = lvl.getInt("remove-after", 5);
        
        HOW_MANY = lvl.getDouble("ile-zabic", 1);
        
        ConfigurationSection mysql = cfg.getConfigurationSection("MySQL");
        
        BASE_NAME = mysql.getString("table-name", "values-base");
        HOST = mysql.getString("host", "localhost");
        USER = mysql.getString("uzytkownik", "root");
        PASSWORD = mysql.getString("haslo", "root");
        DATABASE = mysql.getString("nazwa-bazy", "minectaft");
        
        PORT = mysql.getInt("port", 3306);
        
        USE_MYSQL = mysql.getBoolean("wlacz", false);
        
        ConfigurationSection chat = cfg.getConfigurationSection("chat");
        
        USE_LVL_ON_CHAT = chat.getBoolean("use-lvl-on-chat");
        USE_RANG_ON_CHAT = chat.getBoolean("use-rang-on-chat");
        
        ALLOW_CHAT_LVL = chat.getInt("from-lvl", 15);
        
        TO_REPLACE_LVL = chat.getString("zmiana-poziom", "%lvl%");
        TO_REPLACE_RANG = chat.getString("zmiana-anga", "%rang%");
        
        ConfigurationSection board = cfg.getConfigurationSection("Scoreboard");
        
        ENABLE_SCOREBOARDS = board.getBoolean("enable", false);
        USE_LVL_BELLOW_NAME = board.getBoolean("use-lvl-bellow-name", false);
        USE_RANG_BELLOW_NAME = board.getBoolean("use-rang-as-name-prefix", false);
        NICK_COLOR = board.getString("nick-color", "&f").replaceAll("&", "§");
        ENABLE_SIDEBAR_SCOREBOARDS = board.getBoolean("sidebar.enable", false);
    }
    
    public static ConfigurationSection getConfigurationSection(String patch){
        return cfg.getConfigurationSection(patch);
    }
    
    public static FileConfiguration getConfig(){
        return cfg;
    }
    
    public static void reload(){
        load();
    }
    
}
