package pl.mc4e.ery65.scoreboard;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class SimpleCraftObjective {

    private Map<String, Integer> scores = new HashMap<String, Integer>();
    
    private DisplaySlot slot;
    
    private Objective objective;
    
    private String name, privateName;
    
    public SimpleCraftObjective(Objective obj){
        objective = obj;
        privateName = obj.getName();
    }
    
    private SimpleCraftObjective(final SimpleCraftObjective obj, Objective copy){
        name = obj.name;
        slot = obj.slot;
        privateName = obj.privateName;
        objective = copy;
        scores = obj.scores;
    }
    
    public void setDisplaySlot(DisplaySlot displaySlot){
        slot = displaySlot;
        objective.setDisplaySlot(slot);
    }
    
    public void setDisplayName(String name){
        this.name = name;
        objective.setDisplayName(name);
    }
    
    public DisplaySlot getDisplaySlot(){
        return slot;
    }
    
    public synchronized void addScore(String name, int score){
        scores.put(name, score);
        objective.getScore(Bukkit.getOfflinePlayer(name)).setScore(score);
    }
    
    public synchronized void addScores(Map<String, Integer> scores){
        for (String s : this.scores.keySet()){
            if (!scores.containsKey(s)){
                resetScore(s);
            } else {
                addScore(s, scores.get(s));
                scores.remove(s);
            }
        }
        if (scores == null || scores.isEmpty()){
            return;
        } else {
            for (String s : scores.keySet()){
                addScore(s, scores.get(s));
            }
        }
    }
    
    public synchronized void addScore(Player p, int score){
        scores.put(p.getName(), score);
        objective.getScore(p).setScore(score);
    }
    
    public boolean containsScore(String score){
        return scores.containsKey(score);
    }
    
    public String getName(){
        return name;
    }
    
    public String getPrivateName(){
        return privateName;
    }
    
    public synchronized void unregister(){
        objective.unregister();
    }
    
    public synchronized void resetScore(String score){
        if (scores.containsKey(score)){
            scores.remove(score);
            if (scores == null || scores.isEmpty()){
                scores = new HashMap<String, Integer>();
            }
            objective.getScoreboard().resetScores(Bukkit.getOfflinePlayer(score));
        }
    }
    
    public synchronized void resetScore(OfflinePlayer player){
        if (scores.containsKey(player.getName())){
            scores.remove(player.getName());
            if (scores == null || scores.isEmpty()){
                scores = new HashMap<String, Integer>();
            }
            objective.getScoreboard().resetScores(player);
        }
    }
    
    private synchronized Objective registerNewObjective(Scoreboard board){
        Objective obj = board.registerNewObjective(privateName, "dummy");
        obj.setDisplayName(name);
        obj.setDisplaySlot(slot);
        for (Map.Entry<String, Integer> sc : scores.entrySet()){
            obj.getScore(Bukkit.getOfflinePlayer(sc.getKey())).setScore(sc.getValue());
        }
        return obj;
    }
    
    public synchronized SimpleCraftObjective duplicate(Scoreboard board){
        return new SimpleCraftObjective(this, registerNewObjective(board));
    }
    
}
