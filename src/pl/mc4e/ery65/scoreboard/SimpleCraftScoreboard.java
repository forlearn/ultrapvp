package pl.mc4e.ery65.scoreboard;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class SimpleCraftScoreboard {
    
    private Scoreboard mainScoreboard;
    
    private Map<String, SimpleCraftObjective> objectives = new HashMap<String, SimpleCraftObjective>();
    
    public SimpleCraftScoreboard(Scoreboard sb){
        mainScoreboard = sb;
    }
    
    public synchronized SimpleCraftObjective registerNewSimpleObjective(String name){
        SimpleCraftObjective obj = new SimpleCraftObjective(mainScoreboard.registerNewObjective(name, "dummy"));
        objectives.put(name, obj);
        return obj;
    }
    
    public synchronized void unregisterObjective(SimpleCraftObjective obj){
        if (objectives.containsValue(obj)){
            objectives.remove(obj.getPrivateName());
            obj.unregister();
        }
    }
    
    public Scoreboard getBukkitScoreboard(){
        return mainScoreboard;
    }
    
    public SimpleCraftObjective getObjective(String name){
        return objectives.get(name);
    }
    
    public synchronized void unregisterObjective(String name){
        if (objectives.containsKey(name)){
            objectives.get(name).unregister();
            objectives.remove(name);
        }
    }
    
    private void addObjective(SimpleCraftObjective obj){
        objectives.put(obj.getPrivateName(), obj);
    }
    
    /** without top objective */
    public synchronized SimpleCraftScoreboard copyOfMain(){
        Scoreboard copy = Bukkit.getScoreboardManager().getNewScoreboard();
        SimpleCraftScoreboard coppy = new SimpleCraftScoreboard(copy);
        for (SimpleCraftObjective obj : objectives.values()){
            if (obj.getPrivateName().equalsIgnoreCase("top")){
                continue;
            }
            coppy.addObjective(obj.duplicate(copy));
        }
        for (Team t : mainScoreboard.getTeams()){
            Team team = copy.registerNewTeam(t.getName());
            team.setDisplayName(t.getDisplayName());
            team.setAllowFriendlyFire(t.allowFriendlyFire());
            team.setCanSeeFriendlyInvisibles(t.canSeeFriendlyInvisibles());
            if (t.getPrefix() != null){
                team.setPrefix(t.getPrefix());
            }
            if (t.getSuffix() != null){
                team.setSuffix(t.getSuffix());
            }
            for (OfflinePlayer p : t.getPlayers()){
                team.addPlayer(p);
            }
        }
        return coppy;
    }
    
    /** with top objective */
    public synchronized SimpleCraftScoreboard duplicate(){
        Scoreboard copy = Bukkit.getScoreboardManager().getNewScoreboard();
        SimpleCraftScoreboard coppy = new SimpleCraftScoreboard(copy);
        for (SimpleCraftObjective obj : objectives.values()){
            coppy.addObjective(obj.duplicate(copy));
        }
        for (Team t : mainScoreboard.getTeams()){
            Team team = copy.registerNewTeam(t.getName());
            team.setDisplayName(t.getDisplayName());
            team.setAllowFriendlyFire(t.allowFriendlyFire());
            team.setCanSeeFriendlyInvisibles(t.canSeeFriendlyInvisibles());
            if (t.getPrefix() != null){
                team.setPrefix(t.getPrefix());
            }
            if (t.getSuffix() != null){
                team.setSuffix(t.getSuffix());
            }
            for (OfflinePlayer p : t.getPlayers()){
                team.addPlayer(p);
            }
        }
        return coppy;
    }

}
