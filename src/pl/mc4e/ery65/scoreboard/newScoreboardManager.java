package pl.mc4e.ery65.scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;

import com.google.common.collect.Maps;

public class newScoreboardManager {
    
    private Map<String, Scoreboard> playerBoards = Maps.newHashMap();
    private Map<String, String> translations = Maps.newHashMap();
    
    private Timer tim;
    
    private List<HashMap<String, String>> objectives = new ArrayList<HashMap<String, String>>();
    
    private int currentObjective = 0;
    
    private long currentObjTime = -1;
    private long elapsedTime = 0;
    private long refreshTime = -1;
    
    public newScoreboardManager(){
        tim = new Timer();
        if (PluginConfig.ENABLE_SIDEBAR_SCOREBOARDS){
            encodeSidebar(PluginConfig.getConfigurationSection("Scoreboard.sidebar.scores"));
            if (objectives.size() > 1){
                tim.schedule(new ScoreboardTimer(), 5000, 5000);
            }
        }
    }
    
    public void cancel(){
        tim.cancel();
    }
    
    public void destroy(Player player){
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        if (playerBoards.containsKey(player.getName().toLowerCase())){
            playerBoards.remove(player.getName().toLowerCase());
            if (playerBoards == null || playerBoards.isEmpty()){
                playerBoards = Maps.newHashMap();
            }
        }
    }
    
    public void createNew(Player player, GamePlayer p){
        if (!PluginConfig.ENABLE_SCOREBOARDS)
            return;
        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
        if (PluginConfig.USE_LVL_BELLOW_NAME){
           board = addLvLObjective(board);
           updateAllBellowName(player, p.getLvL());
        }
        if (PluginConfig.USE_RANG_BELLOW_NAME){
            addPlayersToTeams(board);
            addPlayerToTeams(player, p.getRang());
        }
        if (PluginConfig.ENABLE_SIDEBAR_SCOREBOARDS){
           Objective obj = encodeNameAndTimes(board);
           addScores(objectives.get(currentObjective), obj, p);
        }
        player.setScoreboard(board);
        playerBoards.put(player.getName().toLowerCase(), board);
    }
    
    private synchronized Scoreboard addLvLObjective(Scoreboard scoreboard){
        Objective obj = scoreboard.registerNewObjective("lvl", "dummy");
        obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
        obj.setDisplayName(PluginConfig.LVL_NAME);
        addLvLs(obj);
        return scoreboard;
    }
    
    public void updateAllBellowName(Player player, int score){
        for (Map.Entry<String, Scoreboard> psb : playerBoards.entrySet()){
            Player p = Bukkit.getPlayer(psb.getKey());
            Scoreboard sb = psb.getValue();
            Objective obj = sb.getObjective("lvl");
            obj.getScore(player).setScore(score);
            p.setScoreboard(sb);
        }
    }
    
    private synchronized void addPlayersToTeams(Scoreboard board){
        board = UltraPvp.getRang().createRangsInScoreboard(board);
        for (Map.Entry<String, GamePlayer> gp : UltraPvp.getInstance().players.entrySet()){
            GamePlayer p = gp.getValue();
            if (p.getRang() != null)
                board.getTeam(p.getRang()).addPlayer(Bukkit.getPlayer(p.getName()));
        }
    }
    
    private synchronized void addPlayerToTeams(Player player, String team) {
        for (Map.Entry<String, Scoreboard> gp : playerBoards.entrySet()) {
            Scoreboard board = gp.getValue();
            if (!gp.getKey().equalsIgnoreCase(player.getName())){
                addToTeam(player, team, board);
                gp.setValue(board);
                setPlayerScoreboard(gp.getKey(), board);
            }
        }
    }
    
    private void setPlayerScoreboard(String player, Scoreboard board){
        Bukkit.getPlayer(player).setScoreboard(board);
    }
    
    public synchronized void updatePlayerRang(String oldrang, String newrang, Player player){
        for (Map.Entry<String, Scoreboard> gp : playerBoards.entrySet()) {
            Scoreboard board = gp.getValue();
            removeFromTeam(board.getTeam(oldrang), player);
            addToTeam(player, newrang, board);
            setPlayerScoreboard(gp.getKey(), board);
            gp.setValue(board);
        }
    }
    
    private void removeFromTeam(Team team, Player player){
        if (team.hasPlayer(player)){
            team.removePlayer(player);
        }
    }
    
    private void addToTeam(Player p, String team, Scoreboard board){
        if (team == null)
            return;
        Team t = board.getTeam(team);
        if (t != null){
            t.addPlayer(p);
        } else {
            addTeamAndPlayer(p, team, board);
        }
    }
    
    private void addTeamAndPlayer(Player p, String team, Scoreboard board){
        Team t = board.registerNewTeam(team);
        t.setCanSeeFriendlyInvisibles(false);
        t.setAllowFriendlyFire(true);
        t.setPrefix(PluginConfig.RANG_COLOR + team + PluginConfig.NICK_COLOR);
        t.addPlayer(p);
    }
    
    private Objective addLvLs(Objective obj){
        for (Map.Entry<String, GamePlayer> gp : UltraPvp.getInstance().players.entrySet()){
            Player p = Bukkit.getPlayer(gp.getKey());
            obj.getScore(p).setScore(gp.getValue().getLvL());
        }
        obj.getScore(Bukkit.getOfflinePlayer("§bStraznik")).setScore(999999);
        return obj;
    }
    
    private Objective encodeNameAndTimes(Scoreboard board){
        HashMap<String, String> current = objectives.get(currentObjective);
        Objective obj = board.getObjective(currentObjective+"");
        if (obj == null)
            obj = board.registerNewObjective(currentObjective + "", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(current.get("ScoreboardObjectiveName"));
        currentObjTime = parseLong(current.get("ScoreboardObjectiveTime"));
        refreshTime = parseLong(current.get("ScoreboardObjectiveRefreshTime"));
        return obj;
    }
    
    private void addScores(HashMap<String, String> values, Objective obj, GamePlayer player){
        if (values.containsKey("top")){
            addTop(obj, parseTopInt(values.get("top")));
        } else {
            for (Map.Entry<String, String> value : translations.entrySet()){
                if (values.containsKey(value.getValue())){
                    obj.getScore(Bukkit.getOfflinePlayer(value.getValue()))
                    .setScore(player.getFromString(value.getKey()));
                }
            }
        }
    }
    
    public synchronized void update(String how, Player player, GamePlayer gp){
        if (!PluginConfig.ENABLE_SIDEBAR_SCOREBOARDS)
            return;
        Scoreboard board = playerBoards.get(player.getName().toLowerCase());
        if (board == null){
            if (playerBoards.containsKey(player.getName().toLowerCase())){
                return;
            } else {
                createNew(player, UltraPvp.getInstance().players.get(player.getName().toLowerCase()));
                return;
            }
        }
        Objective obj = board.getObjective(DisplaySlot.SIDEBAR);
        HashMap<String, String> codedSidebar = objectives.get(currentObjective);
        if (obj == null){
            return;
        }
        if (how.equalsIgnoreCase("all")){
            if (codedSidebar.containsKey("top")){
                newTop(board, parseTopInt(codedSidebar.get("top")));
            }
            for (Map.Entry<String, String> values : translations.entrySet()){
                if (codedSidebar.containsKey(values.getValue())){
                    obj.getScore(Bukkit.getOfflinePlayer(values.getValue()))
                    .setScore(gp.getFromString(values.getKey()));
                }
            }
        } else {
            if (codedSidebar.containsKey(translations.get(how))){
                obj.getScore(Bukkit.getOfflinePlayer(translations.get(how)))
                .setScore(gp.getFromString(how));
            }
        }
        playerBoards.put(player.getName().toLowerCase(), board);
        player.setScoreboard(board);
    }
    
    private synchronized void updateAll(){
        try {
            Map<String, GamePlayer> players = new HashMap<String, GamePlayer>(UltraPvp.getInstance().players);
            /*Iterator<Entry<String, GamePlayer>> a = UltraPvp.getInstance().players.entrySet().iterator();
            while (a.hasNext()){
                Entry<String, GamePlayer> b = a.next();
                update("all", Bukkit.getPlayer(b.getKey()), b.getValue());
            }*/ 
            for (Map.Entry<String, GamePlayer> gp : players.entrySet()){
                update("all", Bukkit.getPlayer(gp.getKey()), gp.getValue());
            }
        } catch (Exception silent){
            UltraPvp.Error(silent.getStackTrace(), silent.getMessage(), silent);
        }
    }
    
    private void newTop(Scoreboard scoreboard, int howMany){
        clearSidebar(scoreboard);
        Objective obj = encodeNameAndTimes(scoreboard);
        addTop(obj, howMany);
    }
    
    private void addTop(Objective obj, int howMany){
        for (Map.Entry<String, Integer> top : UltraPvp.getDatabaseManager().getxTop(howMany).entrySet()){
            Score s = obj.getScore(Bukkit.getOfflinePlayer(top.getKey()));
            s.setScore(top.getValue());
        }
    }
    
    private int parseTopInt(String s){
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e){
            return 10;
        }
    }
    
    private synchronized void clearSidebar(Scoreboard board){
        if (board.getObjective(DisplaySlot.SIDEBAR) != null){
            board.getObjective(DisplaySlot.SIDEBAR).unregister();
            board.clearSlot(DisplaySlot.SIDEBAR);
        }
    }
    
    private long parseLong(String s){
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException ignored){
            return 60;
        }
    }
    
    private synchronized void changeSidebarObjective(){
        HashMap<String, String> values = objectives.get(currentObjective);
        for (Map.Entry<String, Scoreboard> gp : playerBoards.entrySet()){
            Scoreboard board = gp.getValue();
            clearSidebar(board);
            Objective obj = encodeNameAndTimes(board);
            addScores(values, obj, UltraPvp.getInstance().players.get(gp.getKey()));
            gp.setValue(board);
            setPlayerScoreboard(gp.getKey(), board);
        }
    }
    
    private synchronized void encodeSidebar(ConfigurationSection sidebar){
        if (sidebar == null)
            return;
        for (String keys : sidebar.getKeys(false)){
            HashMap<String, String> values = new HashMap<String, String>();
            values.put("ScoreboardObjectiveTime" ,sidebar.getString(keys + ".time", ""));
            values.put("ScoreboardObjectiveName" ,sidebar.getString(keys + ".name", "&bSidebar").replaceAll("&", "§"));
            values.put("ScoreboardObjectiveRefreshTime" ,sidebar.getString(keys + ".refresh", "-1"));
            for (String key : sidebar.getConfigurationSection(keys +".values").getKeys(false)){
                values.put(key.replaceAll("&", "§"), sidebar.getString(keys + ".values." + key, ""));
            }
            objectives.add(values);
            addTranslations(values);
        }
    }
    
    private void addTranslations(HashMap<String, String> values){
        for (Map.Entry<String, String> map : values.entrySet()){
            if (map.getValue().equalsIgnoreCase("%kills%")){
                translations.put("kills", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%deaths%")){
                translations.put("deaths", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%points%")){
                translations.put("points", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%lvl%")){
                translations.put("lvl", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%money%")){
                translations.put("money", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%killstonextrang%")){
                translations.put("killstonextrang", map.getKey());
            } else if (map.getValue().equalsIgnoreCase("%position%")){
                translations.put("pos", map.getKey());
            }
        }
    }
    
    private class ScoreboardTimer extends TimerTask {

        @Override
        public void run() {
            elapsedTime+= 5;
            if (elapsedTime >= currentObjTime && currentObjTime != -1){
                currentObjective++;
                if (currentObjective == objectives.size())
                    currentObjective = 0;
                changeSidebarObjective();
                elapsedTime = 0;
                return;
            }
            if (refreshTime != -1 && elapsedTime%refreshTime == 0){
                updateAll();
            }
        }
        
    }
    
}