package pl.mc4e.ery65.ultrapvp.api;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import pl.mc4e.ery65.configuration.LangConfiguration;
import pl.mc4e.ery65.configuration.LogsConfig;
import pl.mc4e.ery65.configuration.PluginConfig;
import pl.mc4e.ery65.scoreboard.SimpleCraftScoreboard;
import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;
import pl.minecraft4ever.komendy.Statystyki;

public class UltraPvpAPI {
    
    public GamePlayer getPlayer(String name){
        if (UltraPvp.getInstance().players.containsKey(name.toLowerCase())){
            return UltraPvp.getInstance().players.get(name.toLowerCase());
        } 
        if (MiniManager.isCreatedPlayer(name.toLowerCase())){
            return MiniManager.getCorrectPlayer(new GamePlayer(name.toLowerCase()), Bukkit.getPlayer(name));
        }
        return null;
    }
    
    public FileConfiguration getConfig(){
        return PluginConfig.getConfig();
    }
    
    public FileConfiguration getLangConfig(){
        return LangConfiguration.getConfig();
    }
    
    public boolean containsPlayer(String name){
        if (!UltraPvp.getInstance().players.containsKey(name.toLowerCase())){
            return MiniManager.isCreatedPlayer(name.toLowerCase());
        }
        return true;
    }
    
    public void savePlayer(GamePlayer player){
        MiniManager.save(player);
    }
    
    public int getKills(String name){
        name = name.toLowerCase();
        if (containsPlayer(name)){
            return getPlayer(name).getKills();
        } else
            return 0;
    }
    
    public int getDeaths(String name){
        name = name.toLowerCase();
        if (containsPlayer(name)){
            return getPlayer(name).getDeaths();
        } else
            return 0;
    }
    
    public double getKDR(String name){
        name = name.toLowerCase();
        if (containsPlayer(name)){
            GamePlayer p = getPlayer(name);
            int z = p.getKills();
            int s = p.getDeaths();
            if (z != 0 && s != 0) {
                if (z >= s){
                return Statystyki.round(z / (double) s, 2);
                }
                if (z < s) {
                    return Statystyki.round(z / (double) s, 2);
                }
            }
            if (z == 0 && s > 0) {
                return s;
            }
            if (z > 0 && s == 0) {
                return z;
            }
            if (z == 0 && s == 0) {
                return 1;
            }
        } else
            return 0;
        return 0;
    }
    
    public int getPoints(String name){
        if (containsPlayer(name)){
            return getPlayer(name).getPoints();
        } else
            return 0;
    }
    
    public String getRang(String name){
        if (containsPlayer(name)){
            return getPlayer(name).getRang();
        } else
            return null;
    }
    
    public int getPositionInRank(String name){
        if (containsPlayer(name)){
            return UltraPvp.getDatabaseManager().getPosition(name.toLowerCase());
        } else
            return 0;
    }
    
    public int getLvL(String name){
        if (containsPlayer(name)){
            return getPlayer(name).getLvL();
        } else
            return 0;
    }
    
    public String getPlayerGroup(String name){
        if (containsPlayer(name)){
            return getPlayer(name).getPermission();
        } else
            return "default";
    }
    
    public List<String> getPlayerGroups(Player player){
        return Arrays.asList(UltraPvp.getPermission().getPlayerGroups(player));
    }
    
    public void reloadConfigurations(){
        PluginConfig.reload();
        LogsConfig.reload();
        LangConfiguration.reload();
    }
    
    public void reloadPlugin(){
        reloadConfigurations();
    }
    
    public SimpleCraftScoreboard getMainScoreboard(){
        return UltraPvp.getSimpleScoreboardManager().getMainSb();
    }
    
    public Scoreboard getMainBukkitScoreboard(){
        return UltraPvp.getSimpleScoreboardManager().getMainSb().getBukkitScoreboard();
    }
    
}
