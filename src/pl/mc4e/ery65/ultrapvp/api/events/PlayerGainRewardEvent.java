package pl.mc4e.ery65.ultrapvp.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;
import pl.minecraft4ever.nagrody.Reward;

public class PlayerGainRewardEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    
    private boolean isCanceled = false;
    
    private GamePlayer gp;
    
    private Player p;
    
    private Reward reward;
    
    public PlayerGainRewardEvent(Player player, Reward rew, GamePlayer gamePlayer){
        p = player;
        reward = rew;
        gp = gamePlayer;
    }
    
    public void setReward(Reward r){
        reward = r;
    }
    
    public Reward getReward(){
        return reward;
    }
    
    public Player getPlayer(){
        return p;
    }
    
    public GamePlayer getGamePlayer(){
        return gp;
    }
    
    public boolean isSpecialReward(){
        return UltraPvp.getRewardManager().isSpecialReward(reward);
    }
    
    @Override
    public boolean isCancelled(){
        return isCanceled;
    }

    @Override
    public void setCancelled(boolean toggle){
        isCanceled = toggle;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }
    
    public static HandlerList getHandlerList(){
        return handlers;
    }

}
