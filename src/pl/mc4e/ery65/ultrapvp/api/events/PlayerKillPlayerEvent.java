package pl.mc4e.ery65.ultrapvp.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import pl.minecraft4ever.glowna.GamePlayer;

public class PlayerKillPlayerEvent extends Event {
    
    private static final HandlerList handlers = new HandlerList();
    
    private Player killer, killed;
    
    private GamePlayer gpKiller, gpKilled;
    
    public PlayerKillPlayerEvent(Player killer, Player killed, GamePlayer gpKiller, GamePlayer gpKilled){
        this.gpKilled = gpKilled;
        this.gpKiller = gpKiller;
        this.killed = killed;
        this.killer = killer;
    }
    
    public Player getKiller(){
        return killer;
    }
    
    public Player getKilled(){
        return killed;
    }
    
    public GamePlayer getKillerAsGamePlayer(){
       return gpKiller; 
    }
    
    public GamePlayer getKilledAsGamePlayer(){
        return gpKilled; 
     }
    
    public static HandlerList getHandlerList(){
        return handlers;
    }
    
    @Override
    public HandlerList getHandlers(){
        return handlers;
    }

}
