package pl.mc4e.ery65.ultrapvp.utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;

import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.glowna.UltraPvp;

public class UltraLogger {
    
    private Logger loger;
    
    private boolean isWorking;
    private boolean first = true;
    
    private File file;
    
    private PrintWriter writer;
    
    private Timer tim;
    
    public UltraLogger(Logger logger){
        loger = logger;
    }
    
    public UltraLogger(){
        checkAndCreate();
        tim = new Timer();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date d = cal.getTime();
        tim.schedule(new CheckFile(), d, 86400);
    }
    
    private void checkAndCreate(){
        File dir = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "logs");
        file = new File(dir + File.separator + getFileName());
        if (!file.exists()){
            dir.mkdirs();
            try {
                file.createNewFile();
                writer = new PrintWriter(file);
                isWorking = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (PluginConfig.REMOVE_OLD){
            long last = DateHelper.removeDays(PluginConfig.REMOVE_AFTER).getTime();
            for (File f : dir.listFiles()){
                if (f.lastModified() < last){
                    f.delete();
                }
            }
        }
    }
    
    private String getFileName(){
        String[] a = new Date(System.currentTimeMillis()).toString().split(" ");
        String s = a[2] + "-" + a[1] + "-" + a[5] + ".log";
        return s;
    }
    
    public static void main(String[] args){
        new UltraLogger();
    }
    
    //"[hh:mm:ss]: "
    private String logTime(){
        String date = Calendar.getInstance().getTime().toString();
        date = date.split(" ")[3];
        date = "[" + date + "]: ";
        return date;
    }
    
    public void print(String message){
        String msg = ChatColor.stripColor(message);
        if (loger != null){
            loger.log(Level.INFO, msg);
        } else {
            writer.print(logTime() + msg);
        }
    }
    
    public void println(String message){
        String msg = ChatColor.stripColor(message);
        if (loger != null){
            loger.log(Level.INFO, msg);
        } else {
            writer.println(logTime() + msg);
        }
    }
    
    public void close(){
        if (tim != null){
            tim.cancel();
            writer.close();
            isWorking = false;
        }
    }
    
    private class CheckFile extends TimerTask {
        
        @Override
        public void run() {
            if (first){
                first = false;
            }
            if (isWorking){
                writer.close();
                isWorking = false;
            }
            checkAndCreate();
        }
        
    }
    
}
