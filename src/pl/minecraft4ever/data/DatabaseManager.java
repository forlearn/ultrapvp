package pl.minecraft4ever.data;

import java.util.LinkedHashMap;

import pl.minecraft4ever.glowna.GamePlayer;


public interface DatabaseManager {
    
    public void setTabela(Table tabela);
    
    public void set(Object... wartosci);
    
    public Object get(String index, String toGet, Object wartosci);
    
    public boolean contains(String index, Object wartosci);
    
    public void update(String index, String toUpdate, Object indexwartosci, Object updatewartosci);
    
    public void remove(String index, Object wartosci);
    
    public boolean isOpen();
    
    public void open();
    
    public void close();
    
    public LinkedHashMap<String, Integer> getxTop(int topLimit);
    
    public void resetAllPoints();
    
    public int getPosition(String player);
    
    public void save(GamePlayer g);

}
