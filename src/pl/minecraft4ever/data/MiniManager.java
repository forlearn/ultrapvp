package pl.minecraft4ever.data;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;

public class MiniManager {
    
    public static void update(GamePlayer gamePlayer){
        DatabaseManager man = UltraPvp.getDatabaseManager();
        man.update("nazwa", "poziom", gamePlayer.getName(), gamePlayer.getLvL());
        man.update("Nazwa", "zezwolenie", gamePlayer.getName(), gamePlayer.getZezw());
        man.update("Nazwa", "zabicia", gamePlayer.getName(), gamePlayer.getKills());
        man.update("Nazwa", "smierci", gamePlayer.getName(), gamePlayer.getDeaths());
    }
    
    public static void save(GamePlayer gamePlayer){
        DatabaseManager man = UltraPvp.getDatabaseManager();
        man.update("Nazwa", "poziom", gamePlayer.getName(), gamePlayer.getLvL());
        man.update("Nazwa", "zezwolenie", gamePlayer.getName(), gamePlayer.getZezw());
        man.update("Nazwa", "zabicia", gamePlayer.getName(), gamePlayer.getKills());
        man.update("Nazwa", "smierci", gamePlayer.getName(), gamePlayer.getDeaths());
        man.update("Nazwa", "punkty", gamePlayer.getName(), gamePlayer.getPoints());
        man.update("Nazwa", "ostatnielogowanie", gamePlayer.getName(), days());
    }
    
    public synchronized static GamePlayer getCorrectPlayer(GamePlayer incorrect, Player p){
        if (!isCreatedPlayer(incorrect.getName())){
            String group;
            try {
                group = UltraPvp.getSimpleRewardManager().getMaxPermission(p, UltraPvp.getPermission().getPlayerGroups(Bukkit.getPlayerExact(incorrect.getName())));
            } catch (Exception e){
                group = "default";
            }
            incorrect.SetMaxPermission(group);
            incorrect.setMaxNgr(UltraPvp.getRewardManager().getMaxLvlNgr(incorrect.getLvL(), group));
            //incorrect.setOnceNgr(UltraPvp.getNgrManager().getMaxOnceLvlNgr(incorrect.getLvL(), group));
            if (UltraPvp.getRang().containsBeetwen(incorrect.getLvL())){
                incorrect.setMaxCurrentLvLRang(UltraPvp.getRang().getMaxValue(incorrect.getLvL()));
                incorrect.SetRang(UltraPvp.getRang().getCorrectRang(incorrect.getMaxCurrentRangLvL()));
                incorrect.setNextRangLvL(UltraPvp.getRang().getMaxValue(incorrect.getMaxCurrentRangLvL()+1));
                incorrect.setNextRang(UltraPvp.getRang().getCorrectRang(incorrect.getLvLNextRang()));
            } else {
                incorrect.setMaxCurrentLvLRang(UltraPvp.getRang().getMaxValue(1));
                incorrect.SetRang(UltraPvp.getRang().getCorrectRang(incorrect.getMaxCurrentRangLvL()));
                incorrect.setNextRangLvL(UltraPvp.getRang().getMaxValue(incorrect.getMaxCurrentRangLvL()+1));
                incorrect.setNextRang(UltraPvp.getRang().getCorrectRang(incorrect.getLvLNextRang()));
            }
            ///*if (incorrect.isValid())
                return incorrect;
            //else {
             //   return getCorrectPlayer(incorrect);
            //}*/
        }
        DatabaseManager man = UltraPvp.getDatabaseManager();
        incorrect.setDeaths((int) man.get("Nazwa", "smierci", incorrect.getName()));
        incorrect.setKills((int) man.get("Nazwa", "zabicia", incorrect.getName()));
        incorrect.setLvL((int) man.get("Nazwa", "poziom", incorrect.getName()));
        incorrect.setZezw((int) man.get("Nazwa", "zezwolenie", incorrect.getName()));
        incorrect.setPoints((int) man.get("Nazwa", "punkty", incorrect.getName()));
        if (UltraPvp.getRang().containsBeetwen(incorrect.getLvL())){
            incorrect.setMaxCurrentLvLRang(UltraPvp.getRang().getMaxValue(incorrect.getLvL()));
            incorrect.SetRang(UltraPvp.getRang().getCorrectRang(incorrect.getMaxCurrentRangLvL()));
            incorrect.setNextRangLvL(UltraPvp.getRang().getMaxValue(incorrect.getMaxCurrentRangLvL()+1));
            incorrect.setNextRang(UltraPvp.getRang().getCorrectRang(incorrect.getLvLNextRang()));
        }
        String group;
        try {
            group = UltraPvp.getSimpleRewardManager().getMaxPermission(p, UltraPvp.getPermission().getPlayerGroups(Bukkit.getPlayerExact(incorrect.getName())));
        } catch (Exception e){
            group = "default";
        }
        incorrect.SetMaxPermission(group);
        incorrect.setMaxNgr(UltraPvp.getRewardManager().getMaxLvlNgr(incorrect.getLvL(), group));
        //incorrect.setOnceNgr(UltraPvp.getNgrManager().getMaxOnceLvlNgr(incorrect.getLvL(), group));
        //if (incorrect.isValid())
            return incorrect;
        //else {
        //    return getCorrectPlayer(incorrect);
        //}
    }
    
    public static void create(String name){
        UltraPvp.getDatabaseManager().set(name.toLowerCase(), 0, 0, 0, 0, 0, days());
    }
    
    public static void create(GamePlayer player){
        UltraPvp.getDatabaseManager().set(player.getName().toLowerCase(), player.getLvL(), player.getKills(), 
                player.getDeaths(), player.getZezw(), player.getPoints(), days());
    }
    
    public static void create(int lvl, int kills, int deaths, int zezw, int lastlogin, int points, String name){
        UltraPvp.getDatabaseManager().set(name.toLowerCase(), lvl, kills, deaths, zezw, points, lastlogin);
    }
    
    public static boolean isCreatedPlayer(String name){
        return UltraPvp.getDatabaseManager().contains("Nazwa", name.toLowerCase());
    }
    
    public static int days() {
        Calendar cal = Calendar.getInstance();
        int years = (cal.get(Calendar.YEAR) - 2000) * 365;
        int days = cal.get(Calendar.DAY_OF_YEAR);
        int longYear = (cal.get(Calendar.YEAR) / 4);
        
        return years + days + longYear;
    }

}
