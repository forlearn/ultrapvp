package pl.minecraft4ever.data;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;

import pl.mc4e.ery65.configuration.PluginConfig;


public class MySQLDatabase extends SQLDatabase implements DatabaseManager {
    
    private ConsoleCommandSender cons = Bukkit.getServer().getConsoleSender();
    
    public MySQLDatabase() {
        this.open();
    }
    
    @Override
    public synchronized void open() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad ze sterownikiem MySQL!!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
            return;
        }
        
        String host = PluginConfig.HOST;
        int port = PluginConfig.PORT;
        String database = PluginConfig.DATABASE;
        String user = PluginConfig.USER;
        String pass = PluginConfig.PASSWORD;
        
        try {
            this.con = DriverManager.getConnection("jdbc:mysql://"+host+':'+port+
                    '/'+database, user, pass);
        } catch(SQLException e) {
            cons.sendMessage("§6-----------------------------------------------------");
            cons.sendMessage("§b[§aUltraPvp§b] §cBlad podczas laczenia z baza MySQL!!");
            cons.sendMessage("§6-----------------------------------------------------");
            e.printStackTrace();
        }
    }

}
