package pl.minecraft4ever.data;

public class Table {
    private String nazwa;
    private String uzycie;
    
    public Table(String nazwa, String uzycie) {
        this.nazwa = nazwa;
        this.uzycie = uzycie;
    }
    
    public String getNazwa() {
        return this.nazwa;
    }
    
    public String getUzycie() {
        return " ("+uzycie+")";
    }
    
    public String getWartosci() {
        String wartosc = "";
        String[] a = uzycie.split(",");
        int i = 0;
        for(String b : a) {
            i += 1;
            String[] c = b.split(" ");
            wartosc += c[0] == null ? "" : c[0] + (i <= (a.length -1) ? "," : "");
        }
        
        return "("+wartosc+")";
    }

}
