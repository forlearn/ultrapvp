package pl.minecraft4ever.glowna;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.data.DatabaseManager;
import pl.minecraft4ever.data.MySQLDatabase;
import pl.minecraft4ever.data.SQLDatabase;
import pl.minecraft4ever.data.Table;
import pl.minecraft4ever.nagrody.BetterRewards;
import pl.minecraft4ever.nagrody.RangsManager;
import pl.minecraft4ever.nagrody.Reward;
import pl.minecraft4ever.nagrody.RewardsManager;
import pl.minecraft4ever.nagrody.ngr;

public class FunctionLoader {
    
    /*
    
    ###############################################################################################
    #                                                                                             #
    #                                                                                             #
    #                                            RANGS                                            #
    #                                                                                             #
    #                                                                                             #
    ############################################################################################### 
     
    */
    
    public static synchronized RangsManager getRangsManager(){
        
        File rangs = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rangs.yml");
        
        if (!rangs.exists()){
            checkFiles();
        }
        
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(rangs);
        
        ConfigurationSection rang = cfg.getConfigurationSection("Rangi");
        
        Map<String, String> titles = new HashMap<String, String>();
        
        if (rang == null){
            if (cfg.getString("Ranga1.nazwa") != null){
                int min = 0;
                for (int i = 1 ; i <= 20; i++){
                    String rangName = "Ranga" + i + ".";
                    String name = cfg.getString(rangName + "nazwa"); 
                    int max = cfg.getInt(rangName + "do-poziomu");
                    titles.put(name, min + "-" + max);
                    min = max + 1;
                }
                
                rangs.renameTo(new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rangs.old"));
                
                try {
                    File newrangs = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rangs.yml");
                    PrintWriter zapiska = new PrintWriter(newrangs);
                    zapiska.println("Rangi:");
                    for (String key : titles.keySet()) {
                        zapiska.println("  " + key + ":");
                        zapiska.println("    poziomy: '" + titles.get(key)
                                + "'");
                    }
                    zapiska.close();
                } catch (FileNotFoundException ignored) {
                    // do nothing
                }
                
                return new RangsManager(titles);
            } else {
                return null;
            }
        } else {
            for (String key : rang.getKeys(false)){
                String lvl = rang.getString(key + ".poziomy");
                titles.put(key, lvl);
            }
            return new RangsManager(titles);
        }
    }
    
    
    
    /*
    
    ###############################################################################################
    #                                                                                             #
    #                                                                                             #
    #                                          CHECK FILES                                        #
    #                                                                                             #
    #                                                                                             #
    ############################################################################################### 
     
    */
    
    public static synchronized void checkFiles(){
        File ustawienia = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "config.yml");
        File nagrody = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rewards.yml");
        File ranga = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rangs.yml");
        File better_rewards = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "better-rewards.yml");
        UltraPvp.getInstance().getDataFolder().mkdirs();
        if(!ustawienia.exists()) {
            UltraPvp.getPrivateLogger().println("Creating default config.yml");
            UltraPvp.copy(UltraPvp.getInstance().getResource("Resources/ustawienia.yml"), ustawienia);
        }
        if(!nagrody.exists()) {
            UltraPvp.getPrivateLogger().println("Creating default file rewards.yml");
            UltraPvp.copy(UltraPvp.getInstance().getResource("Resources/nagrody.yml"), nagrody);
        }
        if(!ranga.exists()) {
            UltraPvp.getPrivateLogger().println("Creatings defaul file rangs.yml");
            UltraPvp.copy(UltraPvp.getInstance().getResource("Resources/rangi.yml"), ranga);
        }
        if (!better_rewards.exists()){
            UltraPvp.getPrivateLogger().println("Creatings defaul file better-rewards.yml");
            UltraPvp.copy(UltraPvp.getInstance().getResource("Resources/better-rewards.yml"), better_rewards);
        }
        String jezyczek = PluginConfig.LANG + ".yml";
        File jezyki = new File("plugins/UltraPvp", "lang");
        File jezyk = new File("plugins/UltraPvp/lang", jezyczek);
        if(!jezyk.exists()) {
            jezyki.mkdirs();
            UltraPvp.getPrivateLogger().println("Creating default file "+jezyczek);
            UltraPvp.copy(UltraPvp.getInstance().getResource("Resources/langs/" + jezyczek), jezyk);
        }
    }
    
    /*
      
     ###############################################################################################
     #                                                                                             #
     #                                                                                             #
     #                                            REWARDS                                          #
     #                                                                                             #
     #                                                                                             #
     ############################################################################################### 
      
     */
    
    public synchronized static RewardsManager getRewardsManager(){
        RewardsManager rewmng = new RewardsManager();
        Map<String, ngr> ngrs = new HashMap<String, ngr>();
        File file = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rewards.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        ConfigurationSection ngrpoz = cfg.getConfigurationSection("Nagrody");
        ConfigurationSection ngrpz = cfg.getConfigurationSection("Nagrodaj");
        ConfigurationSection ngrpz1 = cfg.getConfigurationSection("Nagroda");
        if (ngrpoz != null && ngrpz != null) {
            for (String key : cfg.getConfigurationSection("Nagrody").getKeys(
                    false)) {
                List<String> itemy = new ArrayList<String>(), komendy = new ArrayList<String>();
                double kasa = cfg.getDouble("Nagrody." + key + ".Kasa.ilosc");
                boolean useit, usecom, useeco;
                useeco = cfg.getBoolean("Nagrody." + key + ".Kasa.uzywac",
                        true);
                itemy = cfg.getStringList("Nagrody." + key
                        + ".Kit.items");
                useit = cfg.getBoolean("Nagrody." + key + ".Kit.uzywac",
                        false);
                komendy = cfg.getStringList("Nagrody." + key + ".CMD.cmds");
                usecom = cfg.getBoolean("Nagrody." + key + ".CMD.use", false);
                String perm = cfg.getString("Nagrody." + key + ".group",
                        "default");
                rewmng.addPermission(perm.toLowerCase());
                Reward n = Reward.constructReward(itemy, useit, kasa, useeco, komendy, usecom, perm);
                rewmng.addReward(key, n, false);
            }
            for (String key : cfg.getConfigurationSection("Nagrodaj").getKeys(false)) {
                List<String> itemy = new ArrayList<String>(), komendy = new ArrayList<String>();
                double kasa = cfg.getDouble("Nagrodaj." + key + ".Kasa.ilosc");
                boolean useit, usecom, useeco;
                useeco = cfg.getBoolean("Nagrodaj." + key + ".Kasa.uzywac",
                        true);
                itemy = cfg.getStringList("Nagrodaj." + key
                        + ".Kit.items");
                useit = cfg.getBoolean("Nagrodaj." + key + ".Kit.use", false);
                komendy = cfg.getStringList("Nagrodaj." + key + ".CMD.cmds");
                usecom = cfg.getBoolean("Nagrodaj." + key + ".CMD.use", false);
                String perm = cfg.getString("Nagrodaj." + key + ".group",
                        "default");
                rewmng.addPermission(perm.toLowerCase());
                Reward n = Reward.constructReward(itemy, useit, kasa, useeco, komendy, usecom, perm);
                rewmng.addReward(key, n, true);
            }
        } else if (ngrpoz != null && ngrpz == null) {
            for (String klucz : cfg.getConfigurationSection("Nagrody").getKeys(
                    false)) {
                List<String> itemy = new ArrayList<String>(), komendy = new ArrayList<String>();
                double kasa = cfg.getDouble("Nagrody." + klucz + ".Kasa.ilosc");
                boolean useit, usecom, useeco;
                useeco = cfg.getBoolean("Nagrody." + klucz + ".Kasa.uzywac",
                        true);
                itemy = cfg.getStringList("Nagrody." + klucz
                        + ".Kit.items");
                useit = cfg.getBoolean("Nagrody." + klucz + ".Kit.use", false);
                komendy = cfg.getStringList("Nagrody." + klucz + ".CMD.cmds");
                usecom = cfg.getBoolean("Nagrody." + klucz + ".CMD.use", false);
                String perm = cfg.getString("Nagrody." + klucz + ".group",
                        "default");
                Reward n = Reward.constructReward(itemy, useit, kasa, useeco, komendy, usecom, perm);
                rewmng.addReward(klucz, n, false);
            }
        } else {
            ConfigurationSection Na = cfg.getConfigurationSection("nagrody");
            String itii = "";
            double kasaaa = 0.0;
            String cmdss = "";
            boolean usecmdss = false, usekasaaa = false, useprzedmiot = false;
            if (Na != null) {
                List<String> itemy = new ArrayList<String>();
                List<String> komendy = new ArrayList<String>();
                double kasa = cfg.getDouble("nagrody.kasa.ilosc");
                boolean usekasa = cfg.getBoolean("nagrody.kasa.uzycie");
                String item = cfg.getString("nagrody.przedmiot.id");
                int ilosc = cfg.getInt("nagrody.przedmiot.ilosc");
                boolean useit = cfg.getBoolean("nagrody.przedmiot.uzycie");
                String cmd = cfg.getString("nagrody.komenda.komenda");
                boolean usecmd = cfg.getBoolean("nagrody.komenda.uzycie");
                itii = item + " " + ilosc;
                itemy.add(itii);
                komendy.add(cmd);
                kasaaa = kasa;
                cmdss = cmd;
                usecmdss = usecmd;
                useprzedmiot = useit;
                usekasaaa = usekasa;
                Reward n = Reward.constructReward(itemy, useit, kasa, usekasa, komendy, usecmd, "default");
                rewmng.addReward("0-" + PluginConfig.MAX_LVL, n, false);
            }
            if (ngrpz1 != null) {
                for (String key : cfg.getConfigurationSection("Nagroda")
                        .getKeys(false)) {
                    List<String> itemy = new ArrayList<String>();
                    List<String> komendy = new ArrayList<String>();
                    double kasa = cfg.getDouble("Nagroda." + key
                            + ".kasa-ilosc", 5000);
                    boolean usekasa = cfg.getBoolean("Nagroda." + key
                            + ".kasa-uzywac", true);
                    String it = cfg.getString("Nagroda." + key
                            + ".przedmiot-id", "384");
                    boolean useit = cfg.getBoolean("Nagroda." + key
                            + ".przedmiot-uzywac", false);
                    int ile = cfg.getInt("Nagroda." + key + ".przedmiot-ilosc",
                            5);
                    String cmd = cfg.getString("Nagroda." + key
                            + ".komenda-cmd");
                    boolean usecmd = cfg.getBoolean("Nagroda." + key
                            + ".komenda-uzywac", false);
                    boolean zbroja = cfg.getBoolean("Nagroda." + key
                            + ".zbroja-uzywac");
                    itemy.add(it + " " + ile);
                    String permission = cfg.getString("Nagroda." + key
                            + ".group", "default");
                    if (zbroja) {
                        if (PluginConfig.LANG.equalsIgnoreCase("en")) {
                            itemy.add("276 1 knockback:2 fireaspect:2 sharpness:4 name:&cMighty_&5sword_&5for_&a{GRACZ}_&6as_&a{LVL}_&2lvl lore:&2&lhewn_out_by_the_administration");
                            itemy.add("310 1 protection:3 name:&2Reward_&5for_&a{GRACZ}_&6as_&a{LVL}_&2lvl lore:&2&lhewn_out_by_the_administration");
                            itemy.add("311 1 protection:3 name:&2Reward_&5for_&a{GRACZ}_&6as_&a{LVL}_&2lvl lore:&2&lhewn_out_by_the_administration");
                            itemy.add("312 1 protection:3 name:&2Reward_&5for_&a{GRACZ}_&6as_&a{LVL}_&2lvl lore:&2&lhewn_out_by_the_administration");
                            itemy.add("313 1 protection:3 name:&2Reward_&5for_&a{GRACZ}_&6as_&a{LVL}_&2lvl lore:&2&lhewn_out_by_the_administration");
                        } else {
                            itemy.add("276 1 knockback:2 fireaspect:2 sharpness:4 name:&cPotezny_&bmiecz_&5dla_&a{GRACZ}_&6za_&a{LVL}_&2poziom lore:&2&lWykuty_Przez_Administracje");
                            itemy.add("310 1 protection:3 name:&2Nagroda_&5dla_&a{GRACZ}_&6za_&a{LVL}_&2poziom lore:&2&lWykuty_Przez_Administracje");
                            itemy.add("311 1 protection:3 name:&2Nagroda_&5dla_&a{GRACZ}_&6za_&a{LVL}_&2poziom lore:&2&lWykuty_Przez_Administracje");
                            itemy.add("312 1 protection:3 name:&2Nagroda_&5dla_&a{GRACZ}_&6za_&a{LVL}_&2poziom lore:&2&lWykute_Przez_Administracje");
                            itemy.add("313 1 protection:3 name:&2Nagroda_&5dla_&a{GRACZ}_&6za_&a{LVL}_&2poziom lore:&2&lWykute_Przez_Administracje");
                        }
                    }
                    komendy.add(cmd);
                    Reward n = Reward.constructReward(itemy, useit, kasa, usekasa, komendy, usecmd, permission);
                    rewmng.addReward(key, n, true);
                    ngrs.put(key, new ngr(itemy, komendy, useit, usecmd, usekasa, kasa, null, key));
                }
            }
            try {
                Scanner sc = new Scanner(file);
                File newfile = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rewards.old");
                PrintWriter zapis = new PrintWriter(newfile);
                while (sc.hasNextLine()) {
                    zapis.println(sc.nextLine());
                }
                sc.close();
                zapis.close();
                UltraPvp.getPrivateLogger().println("Saving old rewards...");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                UltraPvp.getPrivateLogger().println("An error occured while saving old rewards!!!");
            }

            try {
                File newngrs = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rewards.yml");
                PrintWriter zapiska = new PrintWriter(newngrs);
                zapiska.println("# Domysle nagrody za zdobycie poziomu");
                zapiska.println("# Default rewards for earned lvl");
                zapiska.println("Nagrody:");
                zapiska.println("  '0-10000':");
                zapiska.println("    # Przedmiot otrzymywany za zabicie");
                zapiska.println("    # item received for lvl");
                zapiska.println("    Kit:");
                zapiska.println("      # dawac przedmiot ?");
                zapiska.println("      # give item ?");
                zapiska.println("      uzywac: " + useprzedmiot);
                zapiska.println("      # kity takie same jak w essentials (budowa)");
                zapiska.println("      # kits are the same (constructor) as in essentials");
                zapiska.println("      items:");
                zapiska.println("        - " + itii);
                zapiska.println("    # Kasa otrzymywana za kazdy poziom");
                zapiska.println("    # money received for each lvl");
                zapiska.println("    Kasa:");
                zapiska.println("      # dawac kase ?");
                zapiska.println("      # give money ?");
                zapiska.println("      uzywac: " + usekasaaa);
                zapiska.println("      # ilosc kasy");
                zapiska.println("      # money amount");
                zapiska.println("      ilosc: " + kasaaa);
                zapiska.println("    # komendy za zdobycie kolejnego poziomu");
                zapiska.println("    # commands use for next lvl");
                zapiska.println("    CMD:");
                zapiska.println("      # uzywac komend?");
                zapiska.println("      # use commands?");
                zapiska.println("      use: " + usecmdss);
                zapiska.println("      # komendy (bez " + '"' + "/" + '"'
                        + " )");
                zapiska.println("      # commands (without " + '"' + "/" + '"'
                        + " )");
                zapiska.println("      cmds:");
                zapiska.println("        - " + cmdss);
                zapiska.println("# nagroda za zdobycie podanego lvl");
                zapiska.println("# this is a reward of earned some lvl");
                zapiska.println("Nagrodaj:");
                int x = 1;
                for (String key : ngrs.keySet()) {
                    ngr ng = ngrs.get(key);
                    if (x == 1) {
                        zapiska.println("  # poziom do zdobycia specjalnej nagrody");
                        zapiska.println("  # lvl for earned special reward (in string)");
                        zapiska.println("  '" + key + "':");
                        zapiska.println("    Kit:");
                        zapiska.println("      # dawac przedmity graczowi?");
                        zapiska.println("      # give items for player ?");
                        zapiska.println("      use: " + ng.canUseItems());
                        zapiska.println("      # kity takie same jak w essentials (budowa)");
                        zapiska.println("      # kits are the same (constructor) as in essentials");
                        zapiska.println("      items:");
                        for (int y = 0; y < ng.getItems().size(); y++)
                            zapiska.println("        - " + ng.getItems().get(y));
                        zapiska.println("    Kasa:");
                        zapiska.println("      # kaska - uzywamy? ;)");
                        zapiska.println("      # money - use? ;)");
                        zapiska.println("      uzywac: " + ng.canUseMoney());
                        zapiska.println("      # ilosc kasy");
                        zapiska.println("      # money amount");
                        zapiska.println("      ilosc: " + ng.getMoney());
                        zapiska.println("    CMD:");
                        zapiska.println("      # uzywac komend? :)");
                        zapiska.println("      # use command? :)");
                        zapiska.println("      use: " + ng.canUseCommands());
                        zapiska.println("      # lista komend ;D");
                        zapiska.println("      # commands list;D");
                        zapiska.println("      cmds:");
                        for (int y = 0; y < ng.getCommands().size(); y++)
                            zapiska.println("        - "
                                    + ng.getCommands().get(y));
                        zapiska.println("    # broadcast message for disable write ''");
                        zapiska.println("    # wiadomosc ogloszenie aby wylaczyc wpisz ''");
                        x++;
                        continue;
                    }
                    zapiska.println("  '" + key + "':");
                    zapiska.println("    Kit:");
                    zapiska.println("      use: " + ng.canUseItems());
                    zapiska.println("      items:");
                    for (int y = 0; y < ng.getItems().size(); y++)
                        zapiska.println("        - " + ng.getItems().get(y));
                    zapiska.println("    Kasa:");
                    zapiska.println("      uzywac: " + ng.canUseMoney());
                    zapiska.println("      ilosc: " + ng.getMoney());
                    zapiska.println("    CMD:");
                    zapiska.println("      use: " + ng.canUseCommands());
                    zapiska.println("      cmds:");
                    for (int y = 0; y < ng.getCommands().size(); y++)
                        zapiska.println("        - " + ng.getCommands().get(y));
                }
                zapiska.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        
        return rewmng;
    }
    
    
    /*
    
    ###############################################################################################
    #                                                                                             #
    #                                                                                             #
    #                                      DATABASE-CREATION                                      #
    #                                                                                             #
    #                                                                                             #
    ############################################################################################### 
     
    */
    
    private static synchronized Table getTable(){
        Table tabela = new Table("Konta",
                "Nazwa VARCHAR(50) NOT NULL UNIQUE," + "poziom INT,"
                        + "zabicia INT," + "smierci INT," + "zezwolenie INT," 
                        + "punkty INT," + "ostatnielogowanie INT");
        return tabela;
    }
    
    public static synchronized DatabaseManager getDatabaseManager(){
        DatabaseManager db= getDbManager();
        db.setTabela(getTable());
        return db;
    }
    
    private static synchronized DatabaseManager getDbManager(){
        if (PluginConfig.USE_MYSQL)
            return new MySQLDatabase();
        else
            return new SQLDatabase(UltraPvp.getInstance().getDataFolder().getPath(),
                    PluginConfig.BASE_NAME + ".db");
    }
    
    //Saving special Rewards
    public static synchronized void saveLastSpecialRewards(){
        File lastSpecialRewards = new File(UltraPvp.getInstance().getDataFolder() + File.separator
                + "saves.yml");
        FileConfiguration saves = YamlConfiguration.loadConfiguration(lastSpecialRewards);
        
        saves.createSection("Rewards");
        saves.set("Rewards", UltraPvp.getRewardManager().getLastSpecialRewards());
        
        try {
            saves.save(lastSpecialRewards);
        } catch (IOException ignored) {
            //ignored
        }
    }
    
    public static synchronized void setupLastSpecialRewards(){
        File zapiski = new File(UltraPvp.getInstance().getDataFolder() + File.separator
                + "saves.yml");
        FileConfiguration saves = YamlConfiguration.loadConfiguration(zapiski);
        ConfigurationSection save = saves.getConfigurationSection("Rewards");
        Map<String, Integer> special = new HashMap<String, Integer>();
        if (save != null) {
            for (String name : save.getKeys(false)) {
                int reward = save.getInt(name);
                special.put(name, reward);
            }
        }
        UltraPvp.getRewardManager().setupLastSpecialRewards(special);
    }
    
    //better rewards
    public static synchronized BetterRewards getBetterRewards(){
        BetterRewards rew = new BetterRewards();
        File rewards = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "better-rewards.yml");
        if (!rewards.exists()){
            checkFiles();
        }
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(rewards);
        rew.load(cfg.getConfigurationSection("Groups-rank"), cfg.getConfigurationSection("Diffrences"));
        rew.loadSessionsRewards(PluginConfig.getConfigurationSection("Kills-sessions-rewards"));
        return rew;
    }
    
}