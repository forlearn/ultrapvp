package pl.minecraft4ever.glowna;

import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.nagrody.RewardsManager.SuperSecretClass;

public class GamePlayer {
    
    
    private String name;
    private String maxPerm;
    private String nextRang;
    private String rang;
    
    private SuperSecretClass maxNgr;
    
    private int kills_session;
    private int lvlnextrang = Integer.MIN_VALUE;
    private int maxlvlcurrent = Integer.MIN_VALUE;
    private int lvl = 0;
    private int zezw = 0;
    private int deaths = 0;
    private int kills = 0;
    private int points = 0;
    
    public GamePlayer(String named) {
        name = named.toLowerCase();
    }
    
    public GamePlayer(String name, String ranga) {
        this(name);
        rang = ranga;
    }
    
    public String getName(){
        return name;
    }
    
    public String getRang(){
        return rang;
    }
    
    public String getPermission(){
        return maxPerm;
    }
    
    public int getKills(){
        return kills;
    }
    
    public int getLvL(){
        return lvl;
    }
    
    public int getDeaths(){
        return deaths;
    }
    
    public String getNextRang(){
        return nextRang;
    }
    
    public int getLvLNextRang(){
        return lvlnextrang;
    }
    
    public int getMaxCurrentRangLvL(){
        return maxlvlcurrent;
    }
    
    public int getZezw(){
        return zezw;
    }
    
    public int getPoints(){
        return points;
    }
    
    public int getKillsSession(){
        return kills_session;
    }
    
    @SuppressWarnings("deprecation")
    public int getFromString(String value){
        if (value.equalsIgnoreCase("kills"))
            return kills;
        else if (value.equalsIgnoreCase("deaths"))
            return deaths;
        else if (value.equalsIgnoreCase("lvl"))
            return lvl;
        else if (value.equalsIgnoreCase("killstonextrang"))
            return maxlvlcurrent - lvl;
        else if (value.equalsIgnoreCase("points"))
            return points;
        else if (value.equalsIgnoreCase("pos")){
            return UltraPvp.getDatabaseManager().getPosition(name);
        } else if (value.equalsIgnoreCase("money")){
            return (int) UltraPvp.getEconomy().getBalance(name);
        } else if (value.equalsIgnoreCase("session")){
            return kills_session;
        } else
            return 0;
    }
    
    public SuperSecretClass getMaxNgr(){
        return maxNgr;
    }
    
    public boolean isValid(){
        if (name == null || maxPerm == null || nextRang == null || rang == null
                || maxNgr == null
                || lvlnextrang == Integer.MIN_VALUE || maxlvlcurrent == Integer.MIN_VALUE)
            return false;
        return true;
    }
    
    public void reset(){
        kills = 0;
        deaths = 0;
        points = 0;
        lvl = 0;
        zezw = 0;
        MiniManager.save(this);
    }
    
    public void setKills(int value){
        kills = value;
    }
    
    public void setDeaths(int value){
        deaths = value;
    }
    
    public void setZezw(int value){
        zezw = value;
    }
    
    public void setLvL(int value){
        lvl = value;
    }
    
    public void setPoints(int value){
        points = value;
    }
    
    public void setCorrectLvL(int value){
        zezw = (int)((PluginConfig.DEF_KILLS * value) * PluginConfig.HOW_MANY);
        lvl = value;
    }
    
    public void addLvL(int value){
        lvl += value;
    }
    
    public void addCorrectLvL(int value){
        lvl += value;
        kills = (int) ((kills / PluginConfig.DEF_KILLS) /PluginConfig.HOW_MANY);
        zezw = (int)((PluginConfig.DEF_KILLS * kills) * PluginConfig.HOW_MANY);
    }
    
    public void addZezw(int value){
        zezw += value;
    }
    
    public void addKills(int value){
        kills += value;
    }
    
    public void addPoints(int value){
        points += value;
    }
    
    public void removePoints(int value){
        addPoints(-value);
    }
    
    public void addCorrectKills(int value){
        kills += value;
        zezw = (int)((PluginConfig.DEF_KILLS * kills) * PluginConfig.HOW_MANY);
    }
    
    public void addDeaths(int value){
        deaths += value;
    }
    
    public void makeCorrectZezw(){
        zezw = (int)((PluginConfig.DEF_KILLS * kills) * PluginConfig.HOW_MANY);
    }
    
    public boolean isCorrectZezw(){
        return zezw == (int)((PluginConfig.DEF_KILLS * kills) * PluginConfig.HOW_MANY);
    }
    
    public void SetRang(String value){
        rang = value;
    }
    
    public void setNextRang(String value){
        nextRang = value;
    }
    
    public void setMaxCurrentLvLRang(int maxlvl){
        maxlvlcurrent = maxlvl;
    }
    
    public void setNextRangLvL(int value){
        lvlnextrang = value;
    }
    
    public void setMaxNgr(SuperSecretClass value){
        maxNgr = value;
    }
    
    public void SetMaxPermission(String permission){
        maxPerm = permission;
    }
    
    public void addToSession(int howMany){
        kills_session += howMany;
    }
    
    public void resetKillsSession(){
        kills_session = 0;
    }
    
    public void setKillsSession(int session){
        kills_session = session;
    }
    
}