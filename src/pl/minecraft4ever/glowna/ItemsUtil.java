package pl.minecraft4ever.glowna;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import net.minecraft.util.com.google.common.collect.Maps;

import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.Potion;

public class ItemsUtil {
    
    public static ItemStack ParseItemFromString(String item) {
        String[] info = item.split(" ");
        String ad = info[0];
        short b = 0;
        if (info[0].split(":").length > 1) {
            ad = info[0].split(":")[0];
            b = Short.valueOf(info[0].split(":")[1]);
        }
        @SuppressWarnings("deprecation")
        ItemStack it = new ItemStack(Integer.valueOf(ad), Integer.valueOf(info[1]));
        it.setDurability(b);
        if (info.length > 2){
            if  (it.getItemMeta() instanceof PotionMeta){
                return deserializePotion(it, item);
            } else if (it.getItemMeta() instanceof BookMeta){
                BookMeta meta = (BookMeta) it.getItemMeta();
                meta = deserializeBookMeta(meta, item);
                it.setItemMeta(meta);
                it.addUnsafeEnchantments(deserializeEnchants(info));
                return it;
            } else if (it.getItemMeta() instanceof FireworkMeta){
                FireworkMeta meta = deserializeFirework(item, (FireworkMeta) it.getItemMeta());
                it.setItemMeta(meta);
                return it;
            } else if (it.getItemMeta() instanceof FireworkEffectMeta){
                it.setItemMeta(deserializeFireworkEffect((FireworkEffectMeta) it.getItemMeta(), item));
                it.addUnsafeEnchantments(deserializeEnchants(info));
                return it;
            } else if (it.getItemMeta() instanceof LeatherArmorMeta){
                LeatherArmorMeta meta = (LeatherArmorMeta)it.getItemMeta();
                for (int i = 2; i < info.length; i++){
                    String[] val = info[i].split(":");
                    if (val[0].equalsIgnoreCase("color")){
                        String[] rgb = val[1].split(",");
                        Color c = Color.fromRGB(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]),
                                Integer.parseInt(rgb[2]));
                        meta.setColor(c);
                    } else if (val[0].equalsIgnoreCase("name")){
                        meta.setDisplayName(deserializeName(val[1]));
                    } else if (val[0].equalsIgnoreCase("lore")){
                        meta.setLore(deserializeLore(val[1]));
                    } else 
                        continue;
                }
                it.setItemMeta(meta);
                it.addUnsafeEnchantments(deserializeEnchants(info));
                return it;
            } else if (it.getItemMeta() instanceof SkullMeta){
                SkullMeta meta = (SkullMeta)it.getItemMeta();
                for (int i = 2; i < info.length; i++){
                    String[] val = info[i].split(":");
                    if (val[0].equalsIgnoreCase("player")){
                        meta.setOwner(val[1]);
                    } else if (val[0].equalsIgnoreCase("name")){
                        meta.setDisplayName(deserializeName(val[1]));
                    } else if (val[0].equalsIgnoreCase("lore")){
                        meta.setLore(deserializeLore(val[1]));
                    } else 
                        continue;
                }
                it.setItemMeta(meta);
                it.addUnsafeEnchantments(deserializeEnchants(info));
                return it;
            } else if (it.getItemMeta() instanceof EnchantmentStorageMeta){
                EnchantmentStorageMeta meta = (EnchantmentStorageMeta)it.getItemMeta();
                for (int i = 2; i < info.length; i++){
                    String[] val = info[i].split(":");
                    if (val[0].equalsIgnoreCase("name")){
                        meta.setDisplayName(deserializeName(val[1]));
                    } else if (val[0].equalsIgnoreCase("lore")){
                        meta.setLore(deserializeLore(val[1]));
                    } else 
                        continue;
                }
                for (Map.Entry<Enchantment, Integer> ench : deserializeEnchants(info).entrySet()){
                    meta.addStoredEnchant(ench.getKey(), ench.getValue(), true);
                }
                it.setItemMeta(meta);
                return it;
            } else {
                ItemMeta meta = it.getItemMeta();
                for (int i = 2; i < info.length; i++){
                    String[] val = info[i].split(":");
                    if (val[0].equalsIgnoreCase("name")){
                        meta.setDisplayName(deserializeName(val[1]));
                    } else if (val[0].equalsIgnoreCase("lore")){
                        meta.setLore(deserializeLore(val[1]));
                    } else 
                        continue;
                }
                it.setItemMeta(meta);
                it.addUnsafeEnchantments(deserializeEnchants(info));
                return it;
            }
        } 
        return it;
    }
    
    public static ItemStack[] parseItemStacksFromList(List<String> items){
        ItemStack[] it = new ItemStack[items.size()];
        for (int i = 0; i < items.size(); i++){
            it[i] = ParseItemFromString(items.get(i));
        }
        return it;
    }

    @SuppressWarnings("deprecation")
    public static String ParseItemStackToString(@Nonnull ItemStack it) {
        String item = "" + it.getTypeId();
        if (it.getDurability() != 0)
            item += ":" + it.getDurability();
        item += " " + it.getAmount();
        if (it.hasItemMeta()){
            ItemMeta meta = it.getItemMeta();
            if (meta.hasDisplayName()) {
                String names = meta.getDisplayName();
                names = names.replaceAll("§", "&");
                names = names.replaceAll(" ", "_");
                item += " name:" + names;
            }
            if (meta.hasLore()) {
                item += " lore:";
                for (String s : meta.getLore()) {
                    item += s.replaceAll("§", "&").replaceAll(" ", "_") + "|";
                }
                item = item.substring(0, item.length() - 1);
            }
            if (it.getItemMeta() instanceof PotionMeta){
                Potion p = Potion.fromItemStack(it);
                item += serializePotion(p);
            } else if (it.getItemMeta() instanceof SkullMeta){
                SkullMeta m = (SkullMeta)it.getItemMeta();
                if (m.hasOwner()){
                    item += " player:" + m.getOwner();
                }
            } else if (it.getItemMeta() instanceof EnchantmentStorageMeta){
                EnchantmentStorageMeta m = (EnchantmentStorageMeta) it.getItemMeta();
                item += serializeEnchants(m.getStoredEnchants());
            } else if (it.getItemMeta() instanceof LeatherArmorMeta){
                LeatherArmorMeta m = (LeatherArmorMeta)it.getItemMeta();
                item += " color:" + m.getColor().getRed() + "," + m.getColor().getGreen() + "," +
                m.getColor().getBlue();
            } else if (it.getItemMeta() instanceof FireworkMeta){
                FireworkMeta m = (FireworkMeta)it.getItemMeta();
                item += serializeFirework(m);
            } else if (it.getItemMeta() instanceof BookMeta){
                BookMeta m = (BookMeta)it.getItemMeta();
                item += serializeBook(m);
            } else if (it.getItemMeta() instanceof FireworkEffectMeta){
                FireworkEffectMeta m = (FireworkEffectMeta)it.getItemMeta();
                item += serializeFireworkEffect(m);
            }
            if (meta.hasEnchants()){
                item += serializeEnchants(meta.getEnchants());
            }
        } else {
            if (it.getItemMeta() instanceof PotionMeta){
                Potion p = Potion.fromItemStack(it);
                item += serializePotion(p);
            } else if (it.getItemMeta() instanceof SkullMeta){
                SkullMeta meta = (SkullMeta)it.getItemMeta();
                if (meta.hasOwner()){
                    item += " player:" + meta.getOwner();
                }
            } else if (it.getItemMeta() instanceof EnchantmentStorageMeta){
                EnchantmentStorageMeta meta = (EnchantmentStorageMeta) it.getItemMeta();
                item += serializeEnchants(meta.getStoredEnchants());
            } else if (it.getItemMeta() instanceof LeatherArmorMeta){
                LeatherArmorMeta m = (LeatherArmorMeta)it.getItemMeta();
                item += " color:" + m.getColor().getRed() + "," + m.getColor().getGreen() + "," +
                m.getColor().getBlue();
            } else if (it.getItemMeta() instanceof FireworkMeta){
                FireworkMeta m = (FireworkMeta)it.getItemMeta();
                item += serializeFirework(m);
            } else if (it.getItemMeta() instanceof BookMeta){
                BookMeta m = (BookMeta)it.getItemMeta();
                item += serializeBook(m);
            } else if (it.getItemMeta() instanceof FireworkEffectMeta){
                FireworkEffectMeta m = (FireworkEffectMeta)it.getItemMeta();
                item += serializeFireworkEffect(m);
            }
        }
        return item;
    }
    
    private static BookMeta deserializeBookMeta(BookMeta meta, String serializebook){
        for (String info : serializebook.split(" ")){
            String[] val = info.split(":");
            if (val[0].equalsIgnoreCase("author")){
                meta.setAuthor(val[1]);
            } else if (val[0].equalsIgnoreCase("pages")){
                for (String page : deserializeLore(info.substring(6, info.length()))){
                    meta.addPage(page);
                }
            } else if (val[0].equalsIgnoreCase("name")){
                meta.setDisplayName(deserializeName(val[1]));
            } else if (val[0].equalsIgnoreCase("lore")){
                meta.setLore(deserializeLore(val[1]));
            } else {
                for (Map.Entry<Enchantment, Integer> ench : deserializeEnchants(val).entrySet()){
                    meta.addEnchant(ench.getKey(), ench.getValue(), true);
                }
            }
        }
        return meta;
    }
    
    private static ItemStack deserializePotion(ItemStack it, String serializedPotion){
        Potion p = Potion.fromItemStack(it);
        PotionMeta meta = (PotionMeta)it.getItemMeta();
        for (String info : serializedPotion.split(" ")){
            String[] val = info.split(":");
            if (info.equalsIgnoreCase("splash")){
                p.setSplash(true);
            } else if (info.equalsIgnoreCase("extended")){
                p.setHasExtendedDuration(true);
            } else if (val[0].equalsIgnoreCase("name")){
                meta.setDisplayName(deserializeName(val[1]));
            } else if (val[0].equalsIgnoreCase("lore")){
                meta.setLore(deserializeLore(val[1]));
            } else 
                continue;
        }
        ItemStack its = p.toItemStack(it.getAmount());
        its.addUnsafeEnchantments(deserializeEnchants(serializedPotion.split(" ")));
        return its;
    }
    
    private static FireworkEffectMeta deserializeFireworkEffect(FireworkEffectMeta meta, String fireworkEffect){
        int count = 0;
        String[] ss = fireworkEffect.split("effect:");
        for (String s : ss){
            boolean colors = false;
            if (s == null || s.isEmpty()){
                count++;
                continue;
            }
            Builder builder = FireworkEffect.builder();
            for (String eff : s.split(" ")){
                if (eff.split(":").length == 2){
                    String[] e = eff.split(":");
                    if (e[0].equalsIgnoreCase("color")){
                        colors = true;
                        for (String color : e[1].split(",")){
                            builder.withColor(deserializeColor(color));
                        }
                    } else if (e[0].equalsIgnoreCase("shape")){
                        builder.with(deserializeEffectType(e[1]));
                    } else if (e[0].equalsIgnoreCase("fade")){
                        for (String color : e[1].split(",")){
                            builder.withFade(deserializeColor(color));
                        }
                    } else if (e[0].equalsIgnoreCase("name")){
                        meta.setDisplayName(deserializeName(e[1]));
                    } else if (e[0].equalsIgnoreCase("lore")){
                        meta.setLore(deserializeLore(e[1]));
                    } else {
                        continue;
                    }
                }
            }
            String effect = fireworkEffect.split("effect:")[count].split(" ")[0];
            if (effect.equalsIgnoreCase("twinkle")){
                builder.withFlicker();
            } else if (effect.equalsIgnoreCase("trail")){
                builder.withTrail();
            } else if (effect.equalsIgnoreCase("twinkle,trail")){
               builder.withFlicker().withTrail();
            }
            if (colors)
                meta.setEffect(builder.build());
            count++;
        }
        return meta;
    }
    
    public static FireworkMeta deserializeFirework(String firework, FireworkMeta meta){
        int count = 0;
        String[] ss = firework.split("effect:");
        for (int i =0; i < ss.length;i++){
            boolean colors = false;
            String s = ss[i];
            if (s == null || s.isEmpty()){
                count++;
                continue;
            }
            Builder builder = FireworkEffect.builder();
            for (String eff : s.split(" ")){
                if (eff.split(":").length == 2){
                    String[] e = eff.split(":");
                    if (e[0].equalsIgnoreCase("color")){
                        colors = true;
                        for (String color : e[1].split(",")){
                            builder.withColor(deserializeColor(color));
                        }
                    } else if (e[0].equalsIgnoreCase("shape")){
                        builder.with(deserializeEffectType(e[1]));
                    } else if (e[0].equalsIgnoreCase("fade")){
                        for (String color : e[1].split(",")){
                            builder.withFade(deserializeColor(color));
                        }
                    } else if (e[0].equalsIgnoreCase("power")){
                        meta.setPower(Integer.parseInt(e[1]));
                    } else if (e[0].equalsIgnoreCase("name")){
                        meta.setDisplayName(deserializeName(e[1]));
                    } else if (e[0].equalsIgnoreCase("lore")){
                        meta.setLore(deserializeLore(e[1]));
                    } else {
                        continue;
                    }
                }
            }
            String effect = firework.split("effect:")[count].split(" ")[0];
            if (effect.equalsIgnoreCase("twinkle")){
                builder.withFlicker();
            } else if (effect.equalsIgnoreCase("trail")){
                builder.withTrail();
            } else if (effect.equalsIgnoreCase("twinkle,trail")){
               builder.withFlicker().withTrail();
            }
            if (colors)
                meta.addEffect(builder.build());
            count++;
        }
        return meta;
    }
    
    private static String deserializeName(String name){
        name = name.replaceAll("&", "§").replaceAll("_", " ");
        return name;
    }
    
    private static List<String> deserializeLore(String lore){
        List<String> l = new ArrayList<String>();
        for (String s : lore.split("\\|")){
            l.add(s.replaceAll("&", "§").replaceAll("_", " "));
        }
        return l;
    }
    
    private static Map<Enchantment, Integer> deserializeEnchants(String[] enchants){
        Map<Enchantment, Integer> ench = Maps.newHashMap();
        for (String enchant : enchants){
            int poz;
            if (enchant.split(":").length == 1)
                continue;
            try {
              poz = Integer.parseInt(enchant.split(":")[1]);  
            } catch (NumberFormatException ignored){
                poz = 1;
            }
            String en = enchant.split(":")[0];
            if (en.equalsIgnoreCase("protection"))
                ench.put(Enchantment.PROTECTION_ENVIRONMENTAL, poz);
            if (en.equalsIgnoreCase("projectileprotection"))
                ench.put(Enchantment.PROTECTION_PROJECTILE, poz);
            if (en.equalsIgnoreCase("fireprotection"))
                ench.put(Enchantment.PROTECTION_FIRE, poz);
            if (en.equalsIgnoreCase("featherfalling"))
                ench.put(Enchantment.PROTECTION_FALL, poz);
            if (en.equalsIgnoreCase("blastprotection"))
                ench.put(Enchantment.PROTECTION_EXPLOSIONS, poz);
            if (en.equalsIgnoreCase("respiration"))
                ench.put(Enchantment.WATER_WORKER, poz);
            if (en.equalsIgnoreCase("aquaaffinity"))
                ench.put(Enchantment.OXYGEN, poz);
            if (en.equalsIgnoreCase("sharpness"))
                ench.put(Enchantment.DAMAGE_ALL, poz);
            if (en.equalsIgnoreCase("smite"))
                ench.put(Enchantment.DAMAGE_UNDEAD, poz);
            if (en.equalsIgnoreCase("bane"))
                ench.put(Enchantment.DAMAGE_ARTHROPODS, poz);
            if (en.equalsIgnoreCase("knockback"))
                ench.put(Enchantment.KNOCKBACK, poz);
            if (en.equalsIgnoreCase("fireaspect"))
                ench.put(Enchantment.FIRE_ASPECT, poz);
            if (en.equalsIgnoreCase("looting"))
                ench.put(Enchantment.LOOT_BONUS_MOBS, poz);
            if (en.equalsIgnoreCase("efficiency"))
                ench.put(Enchantment.DIG_SPEED, poz);
            if (en.equalsIgnoreCase("silktouch"))
                ench.put(Enchantment.SILK_TOUCH, poz);
            if (en.equalsIgnoreCase("unbreaking"))
                ench.put(Enchantment.DURABILITY, poz);
            if (en.equalsIgnoreCase("fortune"))
                ench.put(Enchantment.LOOT_BONUS_BLOCKS, poz);
            if (en.equalsIgnoreCase("power"))
                ench.put(Enchantment.ARROW_DAMAGE, poz);
            if (en.equalsIgnoreCase("punch"))
                ench.put(Enchantment.ARROW_KNOCKBACK, poz);
            if (en.equalsIgnoreCase("flame"))
                ench.put(Enchantment.ARROW_FIRE, poz);
            if (en.equalsIgnoreCase("infinity"))
                ench.put(Enchantment.ARROW_INFINITE, poz);
            else
                continue;
        }
        return ench;
    }
    
    private static Color deserializeColor(String color){
        DyeColor c = DyeColor.valueOf(color.toUpperCase());
        return c.getFireworkColor();
    }
    
    private static Type deserializeEffectType(String type){
        switch (type){
        case "ball":
            return Type.BALL;
        case "star":
            return Type.STAR;
        case "creeper":
            return Type.CREEPER;
        case "burst":
            return Type.BURST;
        case "large":
            return Type.BALL_LARGE;
            default:
                return null;
        }
    }
    
    private static String serializeEffect(FireworkEffect effect){
        switch (effect.getType()){
        case BALL:
            return " shape:ball";
        case STAR:
            return " shape:star";
        case CREEPER:
            return " shape:creeper";
        case BURST:
            return  " shape:burst";
        case BALL_LARGE:
            return " shape:large";
            default:
                return "";
        }
    }
    
    private static String serializeprimaryEffect(FireworkEffect effect){
        if (effect.hasFlicker() && !effect.hasTrail()){
            return " effect:twinkle";
        } else if (effect.hasTrail() && !effect.hasFlicker()){
            return " effect:trail";
        } else if (effect.hasFlicker() && effect.hasTrail()){
            return " effect:twinkle,trail";
        } else
            return "";
    }
    
    private static String serializeFireworkEffect(FireworkEffectMeta meta){
        String firework = "";
        if (meta.hasEffect()){
            firework += serializeprimaryEffect(meta.getEffect());
            firework += serializeEffect(meta.getEffect());
            firework += " color:";
            for (Color c : meta.getEffect().getColors()){
                firework += c.toString().toLowerCase() + ",";
            }
            firework = firework.substring(0, firework.length()-1);
            firework += " fade:";
            for (Color c : meta.getEffect().getFadeColors()){
                firework += c.toString().toLowerCase() + ",";
            }
            firework = firework.substring(0, firework.length()-1);
        }
        return firework;
    }
    
    private static String serializeColor(Color c){
        DyeColor cc = DyeColor.getByFireworkColor(c);
        return cc.name().toLowerCase();
    }
    
    private static String serializeFirework(FireworkMeta meta){
        String firework = "";
        if (meta.hasEffects()){
            for (FireworkEffect e : meta.getEffects()){
                firework += serializeprimaryEffect(e);
                firework += " color:";
                for (Color c : e.getColors()){
                    firework += serializeColor(c) + ",";
                }
                firework = firework.substring(0, firework.length()-1);
                firework += " fade:";
                for (Color c : e.getFadeColors()){
                    firework += serializeColor(c) + ",";
                }
                firework = firework.substring(0, firework.length()-1);
                firework += serializeEffect(e);
            }
            firework += " power:" + meta.getPower(); 
        }
        return firework;
    }
    
    private static String serializePotion(Potion potion){
        String p = "";
        if (potion.isSplash())
            p += " splash";
        if (potion.hasExtendedDuration())
            p += " extended";
        return p;
    }
    
    private static String serializeBook(BookMeta meta){
        String book = "";
        if (meta.hasAuthor())
            book += " author:" + meta.getAuthor();
        if (meta.hasPages()){
            book += " pages:";
            for (String s : meta.getPages()){
                book += s.replaceAll("§", "&").replaceAll(" ", "_") + "|";
            }
            book = book.substring(0,book.length()-1);
        }
        return book;
    }
    
    private static String serializeEnchants(Map<Enchantment, Integer> enchants){
        String enchant = "";
        for (Map.Entry<Enchantment, Integer> e : enchants.entrySet()) {
            if (e.getKey().equals(Enchantment.ARROW_DAMAGE)) {
                enchant += " power:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.ARROW_FIRE)) {
                enchant += " flame:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.ARROW_INFINITE)) {
                enchant += " infinity:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.ARROW_KNOCKBACK)) {
                enchant += " punch:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.DAMAGE_ALL)) {
                enchant += " sharpness:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.DAMAGE_ARTHROPODS)) {
                enchant += " bane:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.DAMAGE_UNDEAD)) {
                enchant += " smite:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.DIG_SPEED)) {
                enchant += " efficiency:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.DURABILITY)) {
                enchant += " unbreaking:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.FIRE_ASPECT)) {
                enchant += " fireaspect:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.KNOCKBACK)) {
                enchant += " knockback:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.LOOT_BONUS_BLOCKS)) {
                enchant += " fortune:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.LOOT_BONUS_MOBS)) {
                enchant += " looting:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.OXYGEN)) {
                enchant += " aquaaffinity:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.PROTECTION_ENVIRONMENTAL)) {
                enchant += " protection:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.PROTECTION_EXPLOSIONS)) {
                enchant += " blastprotection:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.PROTECTION_FALL)) {
                enchant += " featherfalling:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.PROTECTION_FIRE)) {
                enchant += " fireprotection:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.PROTECTION_PROJECTILE)) {
                enchant += " projectileprotection:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.SILK_TOUCH)) {
                enchant += " silktouch:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.THORNS)) {
                enchant += " thorns:" + e.getValue();
                continue;
            } else if (e.getKey().equals(Enchantment.WATER_WORKER)) {
                enchant += " respiration:" + e.getValue();
                continue;
            }
        }
        return enchant;
    }
    
}
