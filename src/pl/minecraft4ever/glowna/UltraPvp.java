package pl.minecraft4ever.glowna;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import pl.mc4e.ery65.configuration.PluginConfig;
import pl.mc4e.ery65.scoreboard.SimpleScoreboardManager;
import pl.mc4e.ery65.ultrapvp.api.UltraPvpAPI;
import pl.mc4e.ery65.ultrapvp.utils.UltraLogger;
import pl.minecraft4ever.data.DatabaseManager;
import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.data.SQLThread;
import pl.minecraft4ever.ekonomia.Kasa;
import pl.minecraft4ever.komendy.ResetPointsCommand;
import pl.minecraft4ever.komendy.Statystyki;
import pl.minecraft4ever.komendy.Ustawianiepoziomu;
import pl.minecraft4ever.nagrody.BetterRewards;
import pl.minecraft4ever.nagrody.RangsManager;
import pl.minecraft4ever.nagrody.RewardsManager;
import pl.minecraft4ever.nasluchiwacze.FormatChatu;
import pl.minecraft4ever.nasluchiwacze.dodanielvl;

public class UltraPvp extends JavaPlugin {
    
    private static UltraPvp instance;
    
    private static RangsManager rangsManager;
    
    private static Economy economy = null;
    
    private static Permission permission = null;
    
    private static RewardsManager rewardsManager;
    
    private static DatabaseManager dbmanager;
    
    private static UltraLogger privateLoger;
    
    private static BetterRewards brManager;
    
    private SQLThread sql;
    
    private Timer tim;
    
    private static SimpleScoreboardManager simpleScoreboard;
    
    //private static newScoreboardManager sbmanager;
    
    private static UltraPvpAPI API = new UltraPvpAPI();
    
    public List<String> lockedWorlds = new ArrayList<String>();
    
    public Map<String, GamePlayer> players = new HashMap<String, GamePlayer>();
    
    public void onEnable(){
        instance = this;
        
        load();
        
        setupEconomy();
        
        setupPermissions();
        
        tim = new Timer();
        
        PluginManager pm = this.getServer().getPluginManager();
        
        pm.registerEvents(new dodanielvl(), this);
        pm.registerEvents(new FormatChatu(), this);
        
        getCommand("staty").setExecutor(new Statystyki(this));
        getCommand("poziom").setExecutor(new Ustawianiepoziomu());
        getCommand("kasa").setExecutor(new Kasa());
        getCommand("punkty").setExecutor(new ResetPointsCommand());
        
        getServer().getConsoleSender().sendMessage("§6-------------------------------------------------------------");
        getServer().getConsoleSender().sendMessage("§3Wlaczanie §b[§aUltraPvp§b]§a v "
                + this.getDescription().getVersion() + "§b by §aEry65 ");
        getServer().getConsoleSender().sendMessage("§6-------------------------------------------------------------");
        
        
        tim.schedule(new TimerTask(){
            
            @Override
            public void run() {
                saveAll();
            }
            
        }, PluginConfig.AUTOSAVE_TIME * 1000, PluginConfig.AUTOSAVE_TIME * 1000);
        
        final boolean whitelist = getServer().hasWhitelist();
        
        getServer().setWhitelist(true);
        
        if (!whitelist){
            tim.schedule(new TimerTask(){
                
                @Override
                public void run() {
                    getServer().setWhitelist(whitelist);
                }
                
            }, 30000);
        }
    }
    
    public void onDisable() {
        saveAll();
        
        simpleScoreboard.cancel(true);
        
        ConsoleCommandSender con = Bukkit.getServer().getConsoleSender();
        
        con.sendMessage("§6-------------------------------------------------------------");
        con.sendMessage("§cWylaczanie §b[§aUltraPvp§b]§a v "
                + this.getDescription().getVersion() + "§b by §aEry65 ");
        con.sendMessage("§6-------------------------------------------------------------");
        
        sql.interrupt();
        
        dbmanager.close();
        
    }
    
    private void load(){
        
        privateLoger = new UltraLogger(Bukkit.getLogger());
        
        FunctionLoader.checkFiles();
        
        if (PluginConfig.LOGGING){
            privateLoger = new UltraLogger();
        }
        
        rangsManager = FunctionLoader.getRangsManager();
        
        rewardsManager = FunctionLoader.getRewardsManager();
        
        FunctionLoader.setupLastSpecialRewards();
        
        brManager = FunctionLoader.getBetterRewards();
        
        dbmanager = FunctionLoader.getDatabaseManager();
        
        simpleScoreboard = new SimpleScoreboardManager();
        
        sql = new SQLThread(dbmanager, 300);
        
        sql.start();
        
        //sbmanager = new newScoreboardManager();
        
        if (PluginConfig.LOCKED_WORLDS != null){
            lockedWorlds = new ArrayList<String>();
            for (String s: PluginConfig.LOCKED_WORLDS){
                lockedWorlds.add(s.toLowerCase());
            }
        }
        
        Bukkit.getScheduler().runTaskLater(this, new Runnable() {
            
            public void run() {
                for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                    GamePlayer player = new GamePlayer(p.getName());
                    if (MiniManager.isCreatedPlayer(player.getName())) {
                        player = MiniManager.getCorrectPlayer(player, p);
                    }
                    players.put(player.getName().toLowerCase(), player);
                    /*sbmanager.createNew(p,
                            players.get(p.getName().toLowerCase()));*/
                }
            }
        }, 5L);
        
    }
    
    public static DatabaseManager getDatabaseManager() {
        return dbmanager;
    }
    
    public void destroyScoreboardManager(){
        simpleScoreboard.cancel(true);
        simpleScoreboard = null;
    }
    
    public static void copy(InputStream from, File to) {
        try {
            OutputStream out = new FileOutputStream(to);
            byte[] buffer = new byte[1024];
            int size = 0;
            
            while ((size = from.read(buffer)) != -1) {
                out.write(buffer, 0, size);
            }
            
            out.close();
            from.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /*private synchronized void destroyAll(){
        for (Player p : Bukkit.getOnlinePlayers()){
            sbmanager.destroy(p);
        }
    }*/
    
    @SuppressWarnings("deprecation")
    public static void Error(StackTraceElement[] e, String message, Exception exception){
        File dataFolder = new File(instance.getDataFolder() + File.separator + "/error");
        File file = new File(dataFolder + File.separator + 
                new Date(System.currentTimeMillis()).toLocaleString().replaceAll(":", "_")
                .replaceAll("-", "_").replace(" ", "-") + ".uerr");
        if (!file.exists()){
            dataFolder.mkdirs();
            try {
                file.createNewFile();
            } catch (IOException ignored) {
                //cant make file and save
                return;
            }
        }
        
        try {
            PrintWriter wr = new PrintWriter(file);
            wr.println(message);
            wr.println("");
            wr.println("");
            wr.println("");
            for (int i = 0; i < e.length; i++){
                wr.println(e[i].getLineNumber() + " - " + e[i].getFileName() + " : " 
            + e[i].getMethodName() + e[i].getClassName() 
            + "  ( " + e[i].getClassName() + " at " + e[i].getLineNumber() + " )");
                privateLoger.println(e[i].getLineNumber() + " - " + e[i].getFileName() + " : " 
            + e[i].getMethodName() + e[i].getClassName() 
            + "  ( " + e[i].getClassName() + " at " + e[i].getLineNumber() + " )");
            }
            wr.println("");
            wr.println("");
            wr.println("");
            wr.println("");
            wr.println("");
            wr.println("");
            exception.printStackTrace(wr);
            wr.close();
        } catch (FileNotFoundException ignored) {
            //cant't save info in file
            return;
        }
        
    }
    
    private synchronized void saveAll(){
        try {
            Map<String, GamePlayer> pl = new HashMap<String, GamePlayer>(players);
            for (Map.Entry<String, GamePlayer> gp : pl.entrySet()){
                MiniManager.save(gp.getValue());
            }
        } catch (Exception silent){
            Error(silent.getStackTrace(), silent.getMessage(), silent);
        }
    }
    
    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer()
                .getServicesManager().getRegistration(
                        net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }
    
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer()
                .getServicesManager().getRegistration(
                        net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }
    
    public static UltraPvp getInstance() {
        return instance;
    }
    
    public static SimpleScoreboardManager getSimpleScoreboardManager(){
        return simpleScoreboard;
    }
    
    public static RangsManager getRang() {
        return rangsManager;
    }
    
    public static Economy getEconomy() {
        return economy;
    }
    
    public static Permission getPermission() {
        return permission;
    }
    
    public static RewardsManager getRewardManager() {
        return rewardsManager;
    }
    
    public static UltraLogger getPrivateLogger(){
        return privateLoger;
    }
    
    /*public static newScoreboardManager getScoreboardManager(){
        return sbmanager;
    }*/
    
    public static BetterRewards getSimpleRewardManager(){
        return brManager;
    }
    
    public static UltraPvpAPI getAPI(){
        return API;
    }
    
}