package pl.minecraft4ever.komendy;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import pl.mc4e.ery65.configuration.LangConfiguration;
import pl.minecraft4ever.glowna.UltraPvp;

public class ResetPointsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        
        if (cs.hasPermission("ultrapvp.*") || cs.hasPermission("*")){
            if (args.length == 1){
                if (args[0].equalsIgnoreCase("reset")){
                    UltraPvp.getDatabaseManager().resetAllPoints();
                    cs.sendMessage("§aPomyslnie zresetowales wszystkie punkty graczy!");
                    return true;
                } else {
                    if (UltraPvp.getDatabaseManager().contains("Nazwa", args[0].toLowerCase())){
                        cs.sendMessage("§aGracz §6" + args[0] + " §azebral: §b" + 
                    UltraPvp.getDatabaseManager().get("Nazwa", "punkty", args[0].toLowerCase()) + " §apkt!");
                        return true;
                    } else {
                        cs.sendMessage(LangConfiguration.getWrongPlayer());
                        return true;
                    }
                }
            } else {
                cs.sendMessage("§b/pkt §7[§6reset§7|§6nick§7]");
                return true;
            }
        } else {
            cs.sendMessage(LangConfiguration.getNoPerm());
            return true;
        }
    }

}
