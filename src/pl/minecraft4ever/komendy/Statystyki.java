package pl.minecraft4ever.komendy;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;

public class Statystyki implements CommandExecutor{
    
    @SuppressWarnings("unused")
    private UltraPvp plugin;

    public Statystyki(UltraPvp ultraPvp) {
        this.plugin = ultraPvp;
    }
    
    File con = new File("plugins/UltraPvp", "ustawienia.yml");
    FileConfiguration cfg = YamlConfiguration.loadConfiguration(con);
    String jez = cfg.getString("jezyk", "pl");
    File jezy = new File("plugins/UltraPvp/jezyki", jez+".yml");
    FileConfiguration jezyk = YamlConfiguration.loadConfiguration(jezy);

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        String consola = jezyk.getString("Statystyki.konsola", "&cKomputer i statysktyki &6&lzabic&c?");
        consola = ChatColor.translateAlternateColorCodes('&', consola);
        String noperm = jezyk.getString("No-permission", "&cNie masz uprawnien by uzywac tej komendy");
        String wiad = jezyk.getString("Statystyki.gracz", "&2Twoje statystyki to: &a");
        wiad = ChatColor.translateAlternateColorCodes('&', wiad);
        noperm = ChatColor.translateAlternateColorCodes('&', noperm);
        String zlygracz = jezyk.getString("zly-gracz", "&cPodales zly nick, lub gracza nie ma na serwerze");
        zlygracz = ChatColor.translateAlternateColorCodes('&', zlygracz);
        String wiad1 = jezyk.getString("Statystyki.inny-gracz", "&2Statystyki {GRACZ} to: &a");
        wiad1 = ChatColor.translateAlternateColorCodes('&', wiad1);
        if (label.equalsIgnoreCase("staty") || label.equalsIgnoreCase("stats")){
            if (args.length == 0) {
            if (!(cs instanceof Player)) {
                cs.sendMessage(consola);
                return true;
            }
            Player wysylacz = (Player) cs;
            if (wysylacz.hasPermission("ultrapvp.statystyki")) {
                GamePlayer g = UltraPvp.getInstance().players.get(wysylacz.getName().toLowerCase());
                if (g != null) {
                    int zabicia = g.getKills();
                    int smierci = g.getDeaths();
                    if (zabicia != 0 && smierci != 0) {
                        if (zabicia >= smierci){
                        double srednia = round(zabicia / (double) smierci, 2);
                        cs.sendMessage(wiad  + srednia);
                        }
                        if (zabicia < smierci) {
                            double srednia = round(zabicia / (double) smierci, 2);
                            cs.sendMessage(wiad +"- " + srednia);
                        }
                        return true;
                    }
                    if (zabicia == 0 && smierci > 0) {
                        cs.sendMessage(wiad + "- " + smierci);
                        return true;
                    }
                    if (zabicia > 0 && smierci == 0) {
                        cs.sendMessage(wiad + zabicia);
                        return true;
                    }
                    if (zabicia == 0 && smierci == 0) {
                        cs.sendMessage(wiad + "1");
                        return true;
                    }
                }
                
            }
            else {
                wysylacz.sendMessage(noperm);
            }
            }
            if (args.length == 1) {
                if (cs.hasPermission("ultrapvp.statystyki.inny")){
                if (MiniManager.isCreatedPlayer(args[0])) {
                    GamePlayer d = MiniManager.getCorrectPlayer(new GamePlayer(args[0]), Bukkit.getPlayer(args[0]));
                    int z = d.getKills();
                    int s = d.getDeaths();
                    if (cs.getName() == args[0]) {
                        if (z != 0 && s != 0) {
                            if (z >= s){
                            double srednia = round(z / (double) s, 2);
                            cs.sendMessage(wiad  + srednia);
                            }
                            if (z < s) {
                                double srednia = round(z / (double) s, 2);
                                cs.sendMessage(wiad +" -" + srednia);
                            }
                            return true;
                        }
                        if (z == 0 && s > 0) {
                            cs.sendMessage(wiad + " -" + s);
                            return true;
                        }
                        if (z > 0 && s == 0) {
                            cs.sendMessage(wiad + z);
                            return true;
                        }
                        if (z == 0 && s == 0) {
                            cs.sendMessage(wiad + "1");
                            return true;
                        }
                    }
                    if (z != 0 && s != 0) {
                        if (z >= s){
                        double srednia = round(z / (double) s, 2);
                        wiad1 = wiad1.replace("{GRACZ}", args[0]);
                        cs.sendMessage(wiad1  + srednia);
                        }
                        if (z < s) {
                            double srednia = round(z / (double) s, 2);
                            wiad1 = wiad1.replace("{GRACZ}", args[0]);
                            cs.sendMessage(wiad1 +" -" + srednia);
                        }
                        return true;
                    }
                    if (z == 0 && s > 0) {
                        wiad1 = wiad1.replace("{GRACZ}", args[0]);
                        cs.sendMessage(wiad1 + " -" + s);
                        return true;
                    }
                    if (z > 0 && s == 0) {
                        wiad1 = wiad1.replace("{GRACZ}", args[0]);
                        cs.sendMessage(wiad1 + z);
                        return true;
                    }
                    if (z == 0 && s == 0) {
                        wiad1 = wiad1.replace("{GRACZ}", args[0]);
                        cs.sendMessage(wiad1 + "1");
                        return true;
                    }
                } else {
                    cs.sendMessage(zlygracz);
                    }
                } else {
                    cs.sendMessage(noperm);
                }
            }
        }
            
        return true;
    }
    
    public static double round(double value, int decimals) {
        double p = Math.pow(10, decimals);
        return Math.round(value * p) / p;
    }
    

}
