package pl.minecraft4ever.nagrody;

import org.bukkit.inventory.ItemStack;

public class Nagroda {
    
    private String permission;
    private ItemStack[] items;
    private ItemStack item;
    private double price;
    private String[] commands;
    
    public Nagroda(String perm, String[] comm, double money, ItemStack singleItem, ItemStack... itemy){
        permission = perm;
        commands = comm;
        item = singleItem;
        items = itemy;
        price = money;
    }
    
    public boolean hasCommands(){
        return commands != null;
    }
    
    public boolean hasPermission(){
        return permission != null;
    }
    
    public boolean hasItem(){
        return item != null;
    }
    
    public boolean hasPrice(){
        return price != Double.MIN_VALUE;
    }
    
    public boolean hasItems(){
        return items != null;
    }
    
    public void setPermission(String perm){
        permission = perm;
    }
    
    public void setCommands(String[] cmd){
        commands = cmd;
    }
    
    public void setItem(ItemStack it){
        item = it;
    }
    
    public void setItemS(ItemStack... item){
        items = item;
    }
    
    public String getPermission(){
        return permission;
    }
    
    public String[] getCommands(){
        return commands.clone();
    }
    
    public double getMoney(){
        return price;
    }
    
    public ItemStack getItem(){
        return item.clone();
    }
    
    public ItemStack[] getItems(){
        return items.clone();
    }
    
}
