package pl.minecraft4ever.nagrody;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import pl.mc4e.ery65.configuration.PluginConfig;

public class RangsManager {
    
    private HashMap<Integer, String> ranga = new HashMap<Integer, String>();
    private Map<Integer, RangsLvls> beetwen = new HashMap<Integer, RangsLvls>();
    
    public RangsManager(Map<String, String> rangi) {
        init(rangi);
    }
    
    private synchronized void init(Map<String, String> rangi){
        for (String ranga : rangi.keySet()) {
            String poz = rangi.get(ranga);
            int maxlvl, minlvl;
            String[] dziel = poz.split("-");
            minlvl = Integer.valueOf(dziel[0]);
            maxlvl = minlvl;
            if (dziel.length > 1)
                maxlvl = Integer.valueOf(dziel[1]);
            getRangi(minlvl, maxlvl, ranga);
        }
    }
    
    synchronized void getRangi(int minlvl, int maxlvl, String ranga){
        this.ranga.put(maxlvl, ranga);
        //beetwen.put(minlvl, maxlvl);
        addBeetwen(maxlvl, minlvl);
    }
    
    private void addBeetwen(int max, int min){
        RangsLvls lvl = new RangsLvls(min, max);
        for (int i = min; i <= max; i++){
            beetwen.put(i, lvl);
        }
    }
    
    public boolean containsBeetwen(int lvl){
        if (beetwen.containsKey(lvl)){
            return true;
        }
        return false;
    }
    
    public String getCorrectRang(int lvl){
        /*for (Map.Entry<Integer, Integer> rng : beetwen.entrySet()){
           if (rng.getKey() <= lvl && lvl <= rng.getValue()){
               return ranga.get(rng.getValue());
           }
        }*/
        RangsLvls a = beetwen.get(lvl);
        if (a != null){
            return ranga.get(a.max);
        }
        return null;
    }
    
    public int getMaxValue(int lvl){
        RangsLvls a = beetwen.get(lvl);
        if (a != null){
            return a.max;
        }
        return lvl;
    }
    
    public String getRang(int lvl){
        return ranga.get(lvl);
    }
    
    public Scoreboard createRangsInScoreboard(Scoreboard board){
        for (Map.Entry<Integer, String> rangs : ranga.entrySet()){
            Team t = board.getTeam(rangs.getValue());
            if (t == null)
                t = board.registerNewTeam(rangs.getValue());
            t.setAllowFriendlyFire(true);
            t.setCanSeeFriendlyInvisibles(false);
            //t.setDisplayName(rangs.getValue());
            t.setPrefix(PluginConfig.RANG_COLOR + rangs.getValue() + " " + PluginConfig.NICK_COLOR);
        }
        return board;
    }
    
    private class RangsLvls {
        
        @SuppressWarnings("unused")
        public int max,min;
        
        public RangsLvls(int min, int max){
            this.max = max;
            this.min = min;
        }
    }

}