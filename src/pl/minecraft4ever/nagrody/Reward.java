package pl.minecraft4ever.nagrody;

import java.util.List;

import org.bukkit.inventory.ItemStack;

import pl.mc4e.ery65.configuration.LogsConfig;
import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.glowna.ItemsUtil;
import pl.minecraft4ever.glowna.UltraPvp;

public class Reward {
    
    private String permission;
    private ItemStack[] items;
    private ItemStack item;
    private double price;
    private String[] commands;
    
    public Reward(String perm, String[] comm, double money, ItemStack singleItem, ItemStack... itemy){
        permission = perm;
        commands = comm;
        item = singleItem;
        items = itemy;
        price = money;
    }
    
    public boolean hasCommands(){
        return commands != null;
    }
    
    public boolean hasPermission(){
        return permission != null;
    }
    
    public boolean hasItem(){
        return item != null;
    }
    
    public boolean hasPrice(){
        return price != Double.MIN_VALUE;
    }
    
    public boolean hasItems(){
        return items != null;
    }
    
    public void setPermission(String perm){
        permission = perm;
    }
    
    public void setCommands(String[] cmd){
        commands = cmd;
    }
    
    public void setItem(ItemStack it){
        item = it;
    }
    
    public void setItemS(ItemStack... item){
        items = item;
    }
    
    public void setPrice(double pric){
        price = pric;
    }
    
    public String getPermission(){
        return permission;
    }
    
    public String[] getCommands(){
        return commands.clone();
    }
    
    public double getMoney(){
        return price;
    }
    
    public ItemStack getItem(){
        return item.clone();
    }
    
    public ItemStack[] getItems(){
        return items.clone();
    }
    
    public static Reward constructReward(final List<String> itemy, final boolean useit, final double kasa,
            final boolean usekasa, final List<String> komendy, final boolean usecmd, final String permission){
        if (itemy.size() == 1 && useit){
            if (komendy.size() != 0 && usecmd){
                if (usekasa){
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String commands = "";
                            for (String s : komendy){
                                commands += s + ";";
                            }
                            commands = commands.substring(0, commands.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " commands=(" + commands + ") money=" + kasa + " item=" + itemy.get(0));
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission,  komendy.toArray(new String[0]), kasa,
                            ItemsUtil.ParseItemFromString(itemy.get(0)), null);
                } else {
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String commands = "";
                            for (String s : komendy){
                                commands += s + ";";
                            }
                            commands = commands.substring(0, commands.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " commands=(" + commands + ") item=" + itemy.get(0));
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, komendy.toArray(new String[0]),
                    Double.MIN_VALUE, ItemsUtil.ParseItemFromString(itemy.get(0)), null);
                }
            } else {
                if (usekasa){
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                   + "money=" + kasa + " item=" + itemy.get(0));
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, null, kasa, 
                            ItemsUtil.ParseItemFromString(itemy.get(0)), null);
                } else {
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " item=" + itemy.get(0));
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, null, Double.MIN_VALUE,
                            ItemsUtil.ParseItemFromString(itemy.get(0)), null);
                }
            }
        } else if (itemy.size() > 1 && useit){
            if (komendy.size() != 0 && usecmd){
                if (usekasa){
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String commands = "";
                            for (String s : komendy){
                                commands += s + ";";
                            }
                            commands = commands.substring(0, commands.length() - 1);
                            String items = "";
                            for (String s : itemy){
                                items += s + ";";
                            }
                            items = items.substring(0, items.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " commands=(" + commands + ") money=" + kasa + " items=(" + items + ")");
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, komendy.toArray(new String[0]), kasa,
                            null, ItemsUtil.parseItemStacksFromList(itemy));
                } else {
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String commands = "";
                            for (String s : komendy){
                                commands += s + ";";
                            }
                            commands = commands.substring(0, commands.length() - 1);
                            String items = "";
                            for (String s : itemy){
                                items += s + ";";
                            }
                            items = items.substring(0, items.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " commands=(" + commands + ") items=(" + items + ")");
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, komendy.toArray(new String[0]),
                    Double.MIN_VALUE, null, ItemsUtil.parseItemStacksFromList(itemy));
                }
            } else {
                if (usekasa){
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String items = "";
                            for (String s : itemy){
                                items += s + ";";
                            }
                            items = items.substring(0, items.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " money=" + kasa + " items=(" + items + ")");
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, null, kasa, 
                            null, ItemsUtil.parseItemStacksFromList(itemy));
                } else {
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String items = "";
                            for (String s : itemy){
                                items += s + ";";
                            }
                            items = items.substring(0, items.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " items=(" + items + ")");
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, null, Double.MIN_VALUE,
                            null, ItemsUtil.parseItemStacksFromList(itemy));
                }
            }
        } else {
            if (komendy.size() != 0 && usecmd){
                if (usekasa){
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String commands = "";
                            for (String s : komendy){
                                commands += s + ";";
                            }
                            commands = commands.substring(0, commands.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " commands=(" + commands + ") money=" + kasa);
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, komendy.toArray(new String[0]), kasa,
                            null, null);
                } else {
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            String commands = "";
                            for (String s : komendy){
                                commands += s + ";";
                            }
                            commands = commands.substring(0, commands.length() - 1);
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " commands=(" + commands + ")");
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, komendy.toArray(new String[0]),
                    Double.MIN_VALUE, null, null);
                }
            } else {
                if (usekasa){
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            UltraPvp.getPrivateLogger().println("Constructing new Reward with permission=" + permission 
                                    + " money=" + kasa);
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new Reward");
                        }
                    }
                    return new Reward(permission, null, kasa, 
                            null, null);
                } else {
                    if (PluginConfig.DEBUG){
                        if (LogsConfig.ALL_REW_INFO){
                            UltraPvp.getPrivateLogger().println("Constructing new (wrong) Reward with permission=" + permission);
                        } else {
                            if (LogsConfig.MIN_REW_INFO)
                                UltraPvp.getPrivateLogger().println("Constructing new (wrong) Reward");
                        }
                    }
                    return new Reward(permission, null, Double.MIN_VALUE,
                            null, null);
                }
            } 
        }
    }
    
}
