package pl.minecraft4ever.nagrody;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import pl.mc4e.ery65.configuration.PluginConfig;

import com.google.common.collect.Maps;

public class RewardsManager {
    
    private Map<SuperSecretClass, Reward> lvlngr = Maps.newHashMap();
    private Map<SuperSecretClass, Reward> specialngr = Maps.newHashMap();
    private Map<String, Integer> lastSpecialRewards = new HashMap<String, Integer>();
    
    private Set<String> permissions = new HashSet<String>();
    
    private Set<Integer> lvlList = new HashSet<Integer>();
    
    public RewardsManager(){
        createAlloweds();
    }
    
    private void createAlloweds(){
        int a = PluginConfig.DEF_KILLS;
        double b = PluginConfig.HOW_MANY;
        int d = a;
        int c = PluginConfig.MAX_LVL;
        int ci = c + 1;
        for (int e = 0; e < ci; e++) {
            d = (int) ((a * e) * b);
            lvlList.add(d);
        }
    }
    
    public void setupLastSpecialRewards(Map<String, Integer> rewards){
        lastSpecialRewards = rewards;
    }
    
    public void addPermission(String perm){
        permissions.add(perm.toLowerCase());
    }
    
    public void setupLastSpecialRewards(){
        
    }
    
    public void addReward(String lvls ,Reward ngr, boolean special){
        int max,min;
        if (special){
            try {
                max = Integer.parseInt(lvls.split("-")[0]);
                min = -2;
            } catch (Exception ignored){
                return;
            }
        } else {
            try {
                min = Integer.parseInt(lvls.split("-")[0]);
                max = Integer.parseInt(lvls.split("-")[1]);
            } catch (Exception ignored){
                return;
            }
        }
        if (special){
            SuperSecretClass clas = new SuperSecretClass(ngr.getPermission().toLowerCase(), max, -2);
            if (specialngr.containsKey(clas)){
                if (specialngr.get(clas).equals(ngr)){
                    return;
                }
                addReward((max + 1) + "", ngr, special);
            } else{
                specialngr.put(clas, ngr);
            }
        } else {
            SuperSecretClass clas = new SuperSecretClass(ngr.getPermission().toLowerCase(), max, min);
            if (lvlngr.containsKey(clas)){
                if (lvlngr.get(clas).equals(ngr))
                    return;
                addReward(min + "-" +(max + 1), ngr, special);
            } else
                lvlngr.put(clas, ngr);
        }
    }
    
    public Map<String, Integer> getLastSpecialRewards(){
        return lastSpecialRewards;
    }
    
    public boolean containsAllow(int allow){
        return lvlList.contains(allow);
    }
    
    public boolean containsPermission(String permission){
        return permissions.contains(permission.toLowerCase());
    }
    
    public boolean isSpecialReward(Reward r){
        return specialngr.containsValue(r);
    }
    
    public Reward getReward(SuperSecretClass how, int lvl, String playername){
        playername = playername.toLowerCase();
        SuperSecretClass two = new SuperSecretClass(how.getPermission(), lvl, -2);
        if (specialngr.containsKey(two)){
            if (lastSpecialRewards.containsKey(playername)){
                if (lastSpecialRewards.get(playername) < lvl){
                    lastSpecialRewards.put(playername, lvl);
                    return specialngr.get(two);
                } else {
                    if (lvlngr.containsKey(how)){
                        return lvlngr.get(how);
                    } else {
                        return null;
                    }
                }
            } else {
                lastSpecialRewards.put(playername, lvl);
                return specialngr.get(two);
            }
            
        }
        SuperSecretClass tree = new SuperSecretClass("default", lvl, -2);
        if (specialngr.containsKey(tree)){
            if (lastSpecialRewards.containsKey(playername)){
                if (lastSpecialRewards.get(playername) < lvl){
                    lastSpecialRewards.put(playername, lvl);
                    return specialngr.get(tree);
                } else {
                    if (lvlngr.containsKey(how)){
                        return lvlngr.get(how);
                    } else {
                        return null;
                    }
                }
            } else {
                lastSpecialRewards.put(playername, lvl);
                return specialngr.get(tree);
            }
            
        }
        if (lvlngr.containsKey(how)){
            return lvlngr.get(how);
        }
        return null;
    }
    
    public SuperSecretClass getMaxLvlNgr(int PlayerLevel, String playerGroup){
        for (SuperSecretClass clas : lvlngr.keySet()){
            if (clas.getPermission().equalsIgnoreCase(playerGroup)){
                if (clas.getMinValue() <= PlayerLevel && clas.getMaxValue() >= PlayerLevel){
                    return clas;
                }
            }
        }
        for (SuperSecretClass clas : lvlngr.keySet()){
            if (clas.getPermission().equalsIgnoreCase("default")){
                if (clas.getMinValue() <= PlayerLevel && clas.getMaxValue() >= PlayerLevel){
                    return clas;
                }
            }
        }
        throw new IllegalStateException("There is no rewards for lvl " + PlayerLevel + " with group " + playerGroup);
    }
    
    public static class SuperSecretClass {
        
        private int maxlvl, minlvl;
        private String permission;
        
        public SuperSecretClass(String perm, int maxvalue, int minvalue){
            maxlvl = maxvalue;
            minlvl = minvalue;
            permission = perm;
        }
        
        public String getPermission(){
            return permission;
        }
        
        public int getMinValue(){
            return minlvl;
        }
        
        public int getMaxValue(){
            return maxlvl;
        }
        
        @Override
        public boolean equals(Object obj){
            if (obj == null)
                return false;
            if (!(obj instanceof SuperSecretClass))
                return false;
            final SuperSecretClass f  =(SuperSecretClass) obj;
            if (f.maxlvl != this.maxlvl)
                return false;
            if (f.minlvl != this.minlvl)
                return false;
            if (!f.permission.equalsIgnoreCase(this.permission))
                return false;
            return true;
        }
        
        @Override
        public String toString(){
            return "SuperSecretClass[group: " + permission + ", minlvl: " + 
                    minlvl + " , maxlvl: " + maxlvl + "]";
        }
        
        @Override
        public int hashCode(){
            String lvl = new String(maxlvl + "");
            return permission.hashCode() * permission.length() * lvl.length();
        }
        
    }
    
}
