package pl.minecraft4ever.nagrody;

import java.util.ArrayList;
import java.util.List;

public class ngr {
    
    private List<String> itemy=new ArrayList<String>(), komendy=new ArrayList<String>();
    private double kasa;
    private boolean useit,usecom,useeco;
    private String wiad,name;
    
    public ngr(List<String> itemy, List<String> komendy, boolean useit,
            boolean usecom, boolean useeco, double kasa, String wiad, String name){
        this.itemy = itemy;
        this.kasa = kasa;
        this.komendy = komendy;
        this.usecom = usecom;
        this.useeco = useeco;
        this.wiad = wiad;
        this.useit = useit;
        this.name = name;
    }
    
    public List<String> getItems(){
        return this.itemy;
    }
    
    public List<String> getCommands(){
        return this.komendy;
    }
    
    public double getMoney(){
        return this.kasa;
    }
    
    public boolean canUseMoney(){
        return this.useeco;
    }
    
    public boolean canUseCommands(){
        return this.usecom;
    }
    
    public boolean canUseItems(){
        return this.useit;
    }
    
    public String getMessage(){
        return this.wiad;
    }
    
    public String getName(){
        return this.name;
    }
    
}
