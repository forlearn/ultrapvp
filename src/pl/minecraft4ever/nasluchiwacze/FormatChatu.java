package pl.minecraft4ever.nasluchiwacze;

import java.util.HashMap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import pl.mc4e.ery65.configuration.LangConfiguration;
import pl.mc4e.ery65.configuration.PluginConfig;
import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;


public class FormatChatu implements Listener {
    
    @EventHandler
    public void onPlayer(AsyncPlayerChatEvent e) {
        GamePlayer p;
        if (UltraPvp.getInstance().players.containsKey(e.getPlayer().getName().toLowerCase())){
            p = UltraPvp.getInstance().players.get(e.getPlayer().getName().toLowerCase());
        } else {
            p = MiniManager.getCorrectPlayer(new GamePlayer(e.getPlayer().getName()), e.getPlayer());
        }
        if (p.getLvL() < PluginConfig.ALLOW_CHAT_LVL && !e.getPlayer().isOp()) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(LangConfiguration.getBlockChat());
            return;
        }
        if (e.getFormat().contains(PluginConfig.TO_REPLACE_LVL)
                && e.getFormat().contains(PluginConfig.TO_REPLACE_RANG)) {
            if (UltraPvp.getInstance().lockedWorlds.contains(e.getPlayer()
                    .getWorld().getName().toLowerCase())) {
                e.setFormat(e.getFormat()
                        .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                        .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                return;
            }
            if (PluginConfig.USE_LVL_ON_CHAT && PluginConfig.USE_RANG_ON_CHAT) {
                e.setFormat(e
                        .getFormat()
                        .replaceAll(PluginConfig.TO_REPLACE_LVL, p.getLvL() + "")
                        .replaceAll(PluginConfig.TO_REPLACE_RANG, p.getRang()));
                return;
            } else if (!PluginConfig.USE_LVL_ON_CHAT
                    && PluginConfig.USE_RANG_ON_CHAT) {
                e.setFormat(e.getFormat()
                        .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                        .replaceAll(PluginConfig.TO_REPLACE_RANG, p.getRang()));
                return;
            } else if (PluginConfig.USE_LVL_ON_CHAT
                    && !PluginConfig.USE_RANG_ON_CHAT) {
                e.setFormat(e
                        .getFormat()
                        .replaceAll(PluginConfig.TO_REPLACE_LVL, p.getLvL() + "")
                        .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                return;
            } else {
                e.setFormat(e.getFormat()
                        .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                        .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                return;
            }
        } else {
            if (e.getFormat().contains(PluginConfig.TO_REPLACE_LVL)
                    && !e.getFormat().contains(PluginConfig.TO_REPLACE_RANG)) {
                if (UltraPvp.getInstance().lockedWorlds.contains(e.getPlayer()
                        .getWorld().getName().toLowerCase())) {
                    e.setFormat(e.getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                }
                if (PluginConfig.USE_LVL_ON_CHAT
                        && PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.TO_REPLACE_RANG + " "
                            + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            p.getLvL() + "").replaceAll(
                            PluginConfig.TO_REPLACE_RANG, p.getRang()));
                    return;
                } else if (!PluginConfig.USE_LVL_ON_CHAT
                        && PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.TO_REPLACE_RANG + " "
                            + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            "").replaceAll(PluginConfig.TO_REPLACE_RANG,
                            p.getRang()));
                    return;
                } else if (PluginConfig.USE_LVL_ON_CHAT
                        && !PluginConfig.USE_RANG_ON_CHAT) {
                    e.setFormat(e
                            .getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL,
                                    p.getLvL() + "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                } else {
                    e.setFormat(e.getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                }
            } else if (!e.getFormat().contains(PluginConfig.TO_REPLACE_LVL)
                    && e.getFormat().contains(PluginConfig.TO_REPLACE_RANG)) {
                if (UltraPvp.getInstance().lockedWorlds.contains(e.getPlayer()
                        .getWorld().getName().toLowerCase())) {
                    e.setFormat(e.getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                }
                if (PluginConfig.USE_LVL_ON_CHAT
                        && PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.LVL_NAME + " " + PluginConfig.TO_REPLACE_LVL + " "
                            + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            p.getLvL() + "").replaceAll(
                            PluginConfig.TO_REPLACE_RANG, p.getRang()));
                    return;
                } else if (!PluginConfig.USE_LVL_ON_CHAT
                        && PluginConfig.USE_RANG_ON_CHAT) {
                    e.setFormat(e
                            .getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG,
                                    p.getRang()));
                    return;
                } else if (PluginConfig.USE_LVL_ON_CHAT
                        && !PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.LVL_NAME + " " + PluginConfig.TO_REPLACE_LVL + " "
                            + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            p.getLvL() + "").replaceAll(
                            PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                } else {
                    e.setFormat(e.getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                }
            } else {
                if (UltraPvp.getInstance().lockedWorlds.contains(e.getPlayer()
                        .getWorld().getName().toLowerCase())) {
                    e.setFormat(e.getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                }
                if (PluginConfig.USE_LVL_ON_CHAT
                        && PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.LVL_NAME + " " + PluginConfig.TO_REPLACE_LVL + " " + " "
                            + PluginConfig.TO_REPLACE_RANG + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            p.getLvL() + "").replaceAll(
                            PluginConfig.TO_REPLACE_RANG, p.getRang()));
                    return;
                } else if (!PluginConfig.USE_LVL_ON_CHAT
                        && PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.TO_REPLACE_RANG + " "
                            + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            "").replaceAll(PluginConfig.TO_REPLACE_RANG,
                            p.getRang()));
                    return;
                } else if (PluginConfig.USE_LVL_ON_CHAT
                        && !PluginConfig.USE_RANG_ON_CHAT) {
                    String format = PluginConfig.LVL_NAME + " " + PluginConfig.TO_REPLACE_LVL + " "
                            + e.getFormat();
                    e.setFormat(format.replaceAll(PluginConfig.TO_REPLACE_LVL,
                            p.getLvL() + "").replaceAll(
                            PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                } else {
                    e.setFormat(e.getFormat()
                            .replaceAll(PluginConfig.TO_REPLACE_LVL, "")
                            .replaceAll(PluginConfig.TO_REPLACE_RANG, ""));
                    return;
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        GamePlayer player = new GamePlayer(e.getPlayer().getName());
        player = MiniManager.getCorrectPlayer(player, e.getPlayer());
        UltraPvp.getInstance().players.put(player.getName().toLowerCase(), player);
        UltraPvp.getSimpleScoreboardManager().addPlayer(e.getPlayer(), player);
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        GamePlayer player = UltraPvp.getInstance().players.get(e.getPlayer().getName().toLowerCase());
        if (MiniManager.isCreatedPlayer(player.getName())){
            MiniManager.save(player);
        } else if (player.getKills() != 0 || player.getLvL() != 0 || player.getDeaths() != 0)
            MiniManager.create(player);
        UltraPvp.getInstance().players.remove(player.getName().toLowerCase());
        if (UltraPvp.getInstance().players == null || UltraPvp.getInstance().players.isEmpty()){
            UltraPvp.getInstance().players = new HashMap<String, GamePlayer>();
        }
        UltraPvp.getSimpleScoreboardManager().destroy(e.getPlayer());
    }

}
