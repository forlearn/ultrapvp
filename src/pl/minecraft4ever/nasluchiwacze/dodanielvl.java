package pl.minecraft4ever.nasluchiwacze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import pl.mc4e.ery65.configuration.LangConfiguration;
import pl.mc4e.ery65.configuration.PluginConfig;
import pl.mc4e.ery65.ultrapvp.api.events.PlayerGainRewardEvent;
import pl.mc4e.ery65.ultrapvp.api.events.PlayerKillPlayerEvent;
import pl.minecraft4ever.data.MiniManager;
import pl.minecraft4ever.glowna.GamePlayer;
import pl.minecraft4ever.glowna.UltraPvp;
import pl.minecraft4ever.nagrody.Reward;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.events.PermissionEntityEvent;
import ru.tehkode.permissions.events.PermissionEntityEvent.Action;

public class dodanielvl implements Listener {
    
    private Map<String, String> sesja = new HashMap<String, String>();
    private Timer tim = new Timer();
    
    @EventHandler
    void one(PermissionEntityEvent e){
        if (e.getAction() == Action.DEFAULTGROUP_CHANGED || e.getAction() == Action.INHERITANCE_CHANGED
                || e.getAction() == Action.PERMISSIONS_CHANGED || e.getAction() == Action.TIMEDPERMISSION_EXPIRED){
        }
        if (e.getEntity() instanceof PermissionUser){
            if (UltraPvp.getInstance().players.containsKey(e.getEntity().getName().toLowerCase())){
                PermissionUser u = (PermissionUser)e.getEntity();
                List<String> matched = new ArrayList<String>();
                for (PermissionGroup group : u.getGroups()){
                    if (UltraPvp.getRewardManager().containsPermission(group.getName())){
                        if (!matched.contains(group.getName().toLowerCase())){
                            matched.add(group.getName().toLowerCase());
                        }
                    }
                }
                if (matched.size() == 1){
                    UltraPvp.getInstance().players.get(e.getEntity().getName().toLowerCase())
                    .SetMaxPermission(matched.get(0));
                } else if (matched == null || matched.isEmpty()){
                    UltraPvp.getInstance().players.get(e.getEntity().getName().toLowerCase())
                    .SetMaxPermission("default");
                } else if (matched.size() > 1){
                    if (matched.contains("default")){
                        matched.remove("default");
                    }
                    if (matched.size() == 1){
                        UltraPvp.getInstance().players.get(e.getEntity().getName().toLowerCase())
                        .SetMaxPermission(matched.get(0)); 
                    }
                    UltraPvp.getInstance().players.get(e.getEntity().getName().toLowerCase()).SetMaxPermission(
                            matched.get((new Random().nextInt(matched.size()))));
                }
            }
        }
    }
    
    @EventHandler
    void onPlayerDeath(PlayerDeathEvent e){
        e.setDeathMessage(null);
        Player killed = e.getEntity();
        if (UltraPvp.getInstance().lockedWorlds.contains(killed.getWorld().getName().toLowerCase())){
            return;
        }
        GamePlayer killeds;
        if (UltraPvp.getInstance().players.containsKey(killed.getName().toLowerCase())){
            killeds = UltraPvp.getInstance().players.get(killed.getName().toLowerCase());
        } else {
            killeds = MiniManager.getCorrectPlayer(new GamePlayer(killed.getName()), killed);
        }
        killeds.addDeaths(1);
        killeds.removePoints(1);
        UltraPvp.getSimpleScoreboardManager().update("deaths", killed, killeds);
        UltraPvp.getSimpleScoreboardManager().update("pos", killed, killeds);
        UltraPvp.getSimpleScoreboardManager().update("kills_s", killed, killeds);
        if (killed.getKiller() != null){
            if (killed.getKiller() instanceof Player){
                if (killeds.getKillsSession() > 0){
                    killeds.resetKillsSession();
                } else {
                    killeds.addToSession(-1);
                }
                Player killer = (Player)killed.getKiller();
                if (sesja.containsKey(killer.getName())){
                    int allowed = PluginConfig.KILLS_SESSION;
                    String[] dane = sesja.get(killer.getName()).split(";");
                    String name = dane[0];
                    int current = Integer.valueOf(dane[1]);
                    if (name.equals(killed.getName())){
                        if (current >= allowed)
                            return;
                        else
                            sesja.put(killer.getName(), killed.getName()+";" + (current+1));
                    } else {
                        sesja.put(killer.getName(), killed.getName()+";1");
                    }
                } else{
                    sesja.put(killer.getName(), killed.getName()+";1");
                }
                GamePlayer killers;
                if (UltraPvp.getInstance().players.containsKey(killer.getName().toLowerCase())){
                    killers = UltraPvp.getInstance().players.get(killer.getName().toLowerCase());
                } else {
                    killers = MiniManager.getCorrectPlayer(new GamePlayer(killer.getName()), killer);
                }
                killers.addZezw(1);
                killers.addKills(1);
                killers.addPoints(1);
                UltraPvp.getSimpleScoreboardManager().update("kills", killer, killers);
                UltraPvp.getSimpleScoreboardManager().update("pos", killer, killers);
                if (killers.getLvL() == PluginConfig.MAX_LVL){
                    if (PluginConfig.NGR_IF_MAX_LVL){
                        Reward r = UltraPvp.getSimpleRewardManager().getBetterReward(killers, killeds);
                        PlayerGainRewardEvent evt2 = new PlayerGainRewardEvent(killer, r, killeds);
                        UltraPvp.getInstance().getServer().getPluginManager().callEvent(evt2);
                        if (!evt2.isCancelled()){
                            tim.schedule(new superTimer(killer, killed, r, killers.getLvL()), 50, 50);
                        }
                        /*UltraPvp.getNgrManager().addNgr(killers.getMaxNgr(), killer,
                                killers.getLvL(), killed.getName());*/
                        UltraPvp.getInstance().players.put(killers.getName().toLowerCase(), killers);
                        killer.sendMessage(LangConfiguration.getMaxLvLOn());
                        return;
                    } else {
                        killer.sendMessage(LangConfiguration.getMaxLvLOff());
                        return;
                    }
                }
                if (UltraPvp.getRewardManager().containsAllow(killers.getZezw())){
                    killers.addLvL(1);
                    PlayerKillPlayerEvent evt = new PlayerKillPlayerEvent(killer, killed, killers, killeds);
                    UltraPvp.getInstance().getServer().getPluginManager().callEvent(evt);
                    if (killers.getKillsSession() < 0){
                        killers.resetKillsSession();
                    } else {
                        killers.addToSession(1);
                    }
                    UltraPvp.getSimpleScoreboardManager().updateLvlObjective(killer, killers.getLvL());
                    UltraPvp.getSimpleScoreboardManager().update("lvl", killer, killers);
                    if (killers.getMaxNgr() != null){
                        if (killers.getMaxNgr().getMaxValue() == (killers.getLvL())){
                            killers = MiniManager.getCorrectPlayer(killers, killer);
                        }
                    } else {
                        killers = MiniManager.getCorrectPlayer(killers, killer);
                    }
                    UltraPvp.getSimpleScoreboardManager().update("kills_s", killer, killers);
                    if (killers.getMaxCurrentRangLvL() < killers.getLvL()){
                        if (PluginConfig.USE_RANG_BELLOW_NAME){
                            UltraPvp.getSimpleScoreboardManager().updateRangObjective(killer, killers.getNextRang());
                        }
                        if (UltraPvp.getRang().containsBeetwen(killers.getLvL())){
                            killers.setMaxCurrentLvLRang(killers.getLvLNextRang());
                            killers.SetRang(killers.getNextRang());
                            killers.setNextRangLvL(UltraPvp.getRang().getMaxValue(killers.getLvL()));
                            killers.setNextRang(UltraPvp.getRang().getCorrectRang(killers.getLvLNextRang()));
                        }
                    }
                    Reward r = UltraPvp.getSimpleRewardManager().getBetterReward(killers, killeds);
                    PlayerGainRewardEvent evt2 = new PlayerGainRewardEvent(killer, r, killeds);
                    UltraPvp.getInstance().getServer().getPluginManager().callEvent(evt2);
                    if (!evt2.isCancelled()){
                        tim.schedule(new superTimer(killer, killed, r, killers.getLvL()), 50, 50);
                    }
                    /*UltraPvp.getNgrManager().addNgr(killers.getMaxNgr(), killer,
                            killers.getLvL(), killed.getName());*/
                    UltraPvp.getInstance().players.put(killers.getName().toLowerCase(), killers);
                }
            }
        }
    }
    
    private class superTimer extends TimerTask {
        
        private Player p;
        private String name;
        private Reward n;
        int lvl = 0;
        int i = -2;
        
        public superTimer(Player killer, Player killed, Reward r, int lvl){
            if (killed != null)
                name = killed.getName();
            else
                name = "";
            p = killer;
            lvl = this.lvl;
            n = r;
        }

        @Override
        public void run() {
            if (i == -2){
                addItem();
                i++;
                return;
            }
            if (i == -1){
                addMoney();
                i++;
                return;
            }
            if (commands() && addItems()){
                cancel();
                return;
            }
            i++;
            cancel();
        }
        
        private boolean addItems(){
            if (!n.hasItems()){
                return true;
            }
            if (n.getItems().length <= i){
                return true;
            } else {
                String message = LangConfiguration.getItemNgr().replaceAll("\\{LVL\\}", lvl+"")
                        .replaceAll("%ilosc%", n.getItems()[i].getAmount()+"")
                        .replaceAll("%item%", n.getItems()[i].getType().toString().toLowerCase())
                        .replaceAll("\\{GRACZ\\}", name);
                try {
                    p.getInventory().addItem(n.getItems()[i]);
                    p.sendMessage(message);
                } catch (Exception e){
                    p.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", p.getName()));
                }
                return false;
            }
        }
        
        private void addItem(){
            if (!n.hasItem()){
                return;
            }
            String message = LangConfiguration.getItemNgr().replaceAll("\\{LVL\\}", lvl+"")
                    .replaceAll("%ilosc%", n.getItem().getAmount()+"")
                    .replaceAll("%item%", n.getItem().getType().toString().toLowerCase())
                    .replaceAll("\\{GRACZ\\}", name);
            try {
                p.getInventory().addItem(n.getItem());
                p.sendMessage(message);
            } catch (Exception e){
                p.sendMessage(LangConfiguration.getNoSlotInInventory().replaceAll("%player%", p.getName()));
            }
        }
        
        @SuppressWarnings("deprecation")
        private void addMoney(){
            if (!n.hasPrice()){
                return;
            } else {
                UltraPvp.getEconomy().depositPlayer(p.getName(), n.getMoney());
                p.sendMessage(LangConfiguration.getMoneyNgr().replaceAll("\\{LVL\\}", lvl+"")
                        .replaceAll("%kasa%", n.getMoney()+"").replaceAll("\\{GRACZ\\}", name));
            }
        }
        
        private boolean commands(){
            if (!n.hasCommands()){
                return true;
            }
            if (n.getCommands().length <= i){
                return true;
            } else {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), n.getCommands()[i].replaceAll("\\{GRACZ\\}", p.getName())
                        .replaceAll("\\{LVL\\}", lvl + "").replaceAll("%kasa%", n.getMoney()+"").replaceAll("%player%", p.getName()));
                return false;
            }
        }
        
    }
        
}

